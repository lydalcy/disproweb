<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $mensaje;?>
		</div>
	</div>
</div>
<div class="invoice-content-2">
	<div class="row invoice-head">
		<div class="col-md-7 col-xs-6">
			<div class="invoice-logo">
				<h1 class="uppercase">Factura</h1>
			</div>
		</div>
		<div class="col-md-5 col-xs-6">
			<div class="company-address">
				<span class="bold uppercase">Disproweb C.A.</span>
				<br>
				<span class="bold uppercase">Factura Nº <?php echo $factura->id;?></span>
				<br>
				RIF J-40101414-3
				<br>
				Carrera 17, entre calles 16 y 17
				<br>
				Barquisimeto, Lara
				<br>
				0251 414 6777
				<br>
				info@disproweb.net
				<br>
				www.disproweb.net
			</div>
		</div>
		<div class="row invoice-cust-add">
			<div class="col-xs-3">
				<h2 class="invoice-title uppercase">Cliente</h2>
				<p class="invoice-desc">
					<?php echo $cliente->nombre . ' ' . $cliente->apellido;?>
				</p>
			</div>
			<div class="col-xs-3">
				<h2 class="invoice-title uppercase">Fecha</h2>
				<p class="invoice-desc">
					<?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?>
				</p>
			</div>
			<div class="col-xs-6">
				<h2 class="invoice-title uppercase">Dirección</h2>
				<p class="invoice-desc inv-address">
					<?php echo $cliente->direccion;?>
				</p>
			</div>
		</div>
		<div class="row invoice-body">
			<div class="col-xs-12 table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th class="invoice-title uppercase">Producto</th>
							<th class="invoice-title uppercase text-right">Precio</th>
							<th class="invoice-title uppercase text-right">Subtotal</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$subtotal_dominios = 0;
						foreach ($detalles_dominios as $dominio) {
							?>
							<tr>
								<td>
									<h3><?php echo $dominio->dominio;?></h3>
									<p>
										<?php echo $dominio->tipo;?>
									</p>
								</td>
								<td class="text-right sbold">
									<?php
									switch ($dominio->tipo) {
										case 'Registro': {
											$precio_dominio = $dominio->precio_registro;
											break;
										}
										case 'Transferencia': {
											$precio_dominio = $dominio->precio_transferencia;
											break;
										}
										case 'Propio': {
											$precio_dominio = $dominio->precio_propio;
											break;
										}
									}
									?>
									Bs. <?php echo number_format($precio_dominio, 2, ',', '.');?>
								</td>
								<td class="text-right sbold">
									<?php
									$subtotal_dominios += $precio_dominio;
									?>
									Bs. <?php echo number_format($precio_dominio, 2, ',', '.');?>
								</td>
							</tr>
							<?php
						}
						$subtotal_planes = 0;
						foreach ($detalles_planes as $plan) {
							?>
							<tr>
								<td>
									<h3><?php echo $plan->nombre;?></h3>
									<p>
										<?php echo $plan->descripcion;?>
									</p>
									<p>
										<?php echo $plan->ciclo_facturacion;?>
									</p>
								</td>
								<td class="text-right sbold">
									<?php
									switch ($plan->ciclo_facturacion) {
										case 'Mensual': {
											$precio_plan = $plan->precio_mensual;
											break;
										}
										case 'Trimestral': {
											$precio_plan = $plan->precio_trimestral;
											break;
										}
										case 'Semestral': {
											$precio_plan = $plan->precio_semestral;
											break;
										}
										case 'Anual': {
											$precio_plan = $plan->precio_anual;
											break;
										}
									}
									?>
									Bs. <?php echo number_format($precio_plan, 2, ',', '.');?>
								</td>
								<td class="text-right sbold">
									<?php
									$subtotal_planes += $precio_plan;
									?>
									Bs. <?php echo number_format($precio_plan, 2, ',', '.');?>
								</td>
							</tr>
							<?php
						}
						$subtotal_general = $subtotal_dominios + $subtotal_planes;
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row invoice-subtotal">
			<div class="col-md-10 text-right">
				<p class="invoice-desc grand-total">Subtotal</p>
				<p class="invoice-desc grand-total">Descuento</p>
				<p class="invoice-desc grand-total">Subtotal - Descuento</p>
				<p class="invoice-desc grand-total">IVA (<?php echo $iva->iva;?>%)</p>
				<p class="invoice-desc grand-total">Total</p>
			</div>
			<div class="col-md-2 text-right">
				<p class="invoice-desc grand-total">
					Bs. <?php echo number_format($subtotal_general, 2, ',', '.');?>
				</p>
				<p class="invoice-desc grand-total">
					Bs. 
					<?php
					echo number_format($factura->descuento, 2, ',', '.');
					?>
				</p>
				<p class="invoice-desc grand-total">
					Bs. 
					<?php
					$subtotal_descuento = $subtotal_general - $factura->descuento;
					echo number_format($subtotal_descuento, 2, ',', '.');
					?>
				</p>
				<p class="invoice-desc grand-total">
					Bs. 
					<?php
					$total_iva = $subtotal_descuento * ($iva->iva / 100);
					echo number_format($total_iva, 2, ',', '.');
					?>
				</p>
				<p class="invoice-desc grand-total">
					Bs. 
					<?php
					$total = $subtotal_general + $total_iva - $factura->descuento;
					echo number_format($total, 2, ',', '.');
					?>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a class="btn green-haze" href="<?php echo base_url('reportes/facturaPdf/' . $this->uri->segment(3) . '/' . $this->uri->segment(4));?>" target="_blank">
					Generar PDF
				</a>
				<a class="btn green-haze" href="<?php echo base_url('pagos/reportar');?>">
					Pagar mediante transferencia o depósito
				</a>
			</div>
		</div>
	</div>
</div>