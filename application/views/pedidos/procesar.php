<div class="row">
	<div class="col-md-12">
		<div role="tabpanel">
			<?php
			if ($this->session->flashdata('error')) {
				echo $this->session->flashdata('error');
			}
			if ($this->session->flashdata('mensaje')) {
				?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $this->session->flashdata('mensaje');?>
				</div>
				<?php
			}
			?>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#nuevo" aria-controls="nuevo" role="tab" data-toggle="tab">
						Nuevo usuario
					</a>
				</li>
				<li role="presentation">
					<a href="#existente" aria-controls="existente" role="tab" data-toggle="tab">
						Usuario existente
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="nuevo">
					<form action="<?php echo base_url('clientes/guardar');?>" method="post">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="usuario">Cédula</label>
									<input type="text" name="cedula" id="cedula" class="form-control" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="usuario">Usuario</label>
									<input type="text" name="usuario" id="usuario" class="form-control" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="password">Contraseña</label>
									<input type="password" name="password" id="password" class="form-control" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="nombre">Nombre</label>
									<input type="text" name="nombre" id="nombre" class="form-control" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="apellido">Apellido</label>
									<input type="text" name="apellido" id="apellido" class="form-control" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="telefono">Teléfono</label>
									<input type="text" name="telefono" id="telefono" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="correo">Correo</label>
									<input type="email" name="correo" id="correo" class="form-control" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="direccion">Dirección</label>
									<textarea name="direccion" id="direccion" cols="30" rows="5" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-primary" type="submit">
									<span class="glyphicon glyphicon-ok"></span>
									Registrarse y procesar pedido
								</button>
							</div>
						</div>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="existente">
					<div class="row">
						<div class="col-md-6">
							<form action="<?php echo base_url('clientes/login');?>" method="post">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="usuario">Usuario</label>
											<input type="text" name="usuario" id="usuario" class="form-control" required>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="password">Contraseña</label>
											<input type="password" name="password" id="password" class="form-control" required>
										</div>
									</div>
									<div class="col-md-12">
									<input type="hidden" name="accion" value="pedido">
										<button class="btn btn-primary" type="submit">
											<span class="glyphicon glyphicon-ok"></span>
											Iniciar sesión y procesar pedido
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>