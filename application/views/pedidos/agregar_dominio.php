<div class="row">
    <div class="col-md-12">
        <form action="<?php echo base_url('pedidos/agregar');?>" method="post" role="form">
            <?php
            if ($this->session->flashdata('error')) {
                echo $this->session->flashdata('error');
            }
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">www.</div>
                            <input type="text" name="dominio" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="input-group-btn">
                            <select name="extension" class="form-control">
                                <?php
                                foreach ($dominios as $dominio) {
                                    ?>
                                    <option value="<?php echo $dominio->id;?>">
                                        <?php echo $dominio->extension;?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="hosting" value="<?php echo $hosting;?>">
                    <button type="submit" class="btn btn-primary">
                        Continuar al carrito
                        <span class="icon-arrow-right-circle"></span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>