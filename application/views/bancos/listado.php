<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-condensed table-striped datatables-dominios">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Nro. Cuenta</th>
			<th>Razón Social</th>
			<th>RIF</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($bancos as $banco) {
			?>
			<tr>
				<td>
					<?php echo $banco->nombre;?>
				</td>
				<td>
					<?php echo $banco->nro_cuenta;?>
				</td>
				<td>
					<?php echo $banco->razon_social;?>
				</td>
				<td>
					<?php echo $banco->rif;?>
				</td>
				<td>
					<?php
					if ($this->session->userdata('nivel') == 1) {
						?>
						<a href="<?php echo base_url('bancos/editar/' . $banco->id);?>" data-tooltip="tooltip" data-placement="top" title="Editar">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>
						<a href="#modal-confirmacion" data-route="<?php echo base_url('bancos/eliminar/' . $banco->id);?>" data-tooltip="tooltip" data-toggle="modal" data-placement="top" title="Eliminar">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>