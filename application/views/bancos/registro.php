<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
?>
<form action="<?php echo base_url('bancos/guardar');?>" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="nro_cuenta">Nro. de Cuenta</label>
				<input type="text" name="nro_cuenta" id="nro_cuenta" class="form-control" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="razon_social">Razón Social</label>
				<input type="text" name="razon_social" id="razon_social" class="form-control" required>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="rif">RIF</label>
				<input type="text" name="rif" id="rif" class="form-control" required>
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-ok"></span>
				Registrar
			</button>
		</div>
	</div>
</form>