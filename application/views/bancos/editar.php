<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<form action="<?php echo base_url('bancos/modificar');?>" method="post">
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required value="<?php echo $banco->nombre;?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="nro_cuenta">Nro. de Cuenta</label>
				<input type="text" name="nro_cuenta" id="nro_cuenta" class="form-control" required value="<?php echo $banco->nro_cuenta;?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="razon_social">Razón Social</label>
				<input type="text" name="razon_social" id="razon_social" class="form-control" required value="<?php echo $banco->razon_social;?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="rif">RIF</label>
				<input type="text" name="rif" id="rif" class="form-control" required value="<?php echo $banco->rif;?>">
			</div>
		</div>
		<div class="col-md-12">
			<input type="hidden" name="id"  value="<?php echo $banco->id;?>">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-refresh"></span>
				Modificar
			</button>
		</div>
	</div>
</form>