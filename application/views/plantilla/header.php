<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $titulo;?></title>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/simple-line-icons/css/simple-line-icons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/layout.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/yellow-orange.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/components.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/pricing.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/invoice-2.min.css');?>">
</head>
<body class="page-container-bg-solid page-boxed">
	<div class="page-header">
		<div class="page-header-top">
			<div class="container">
				<div class="page-logo">
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url('assets/img/logo.png');?>" alt="logo" class="logo-default" width="120px">
					</a>
				</div>
				<a href="javascript:;" class="menu-toggler"></a>
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<?php
						if ($this->session->userdata('cliente_id')) {
							?>
							<li>
								<a href="<?php echo base_url('clientes/logout');?>">
									<i class="icon-logout"></i> Salir
								</a>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
		</div>
		<div class="page-header-menu">
			<div class="container">
				<nav class="hor-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="<?php echo base_url();?>">
								Hospedaje Web
							</a>
						</li>
						<li>
							<a href="<?php echo base_url('pedidos/dominio');?>"> 
								Registro de Dominios
							</a>
						</li>
						<li>
							<a href="<?php echo base_url('pedidos/carrito');?>">
								Carrito de compras
							</a>
						</li>
						<li class="menu-dropdown classic-menu-dropdown ">
							<a href="javascript:;"> Pagos
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu pull-left">
								<li>
									<a href="<?php echo base_url('bancos');?>" class="nav-link">
										Ver datos bancarios
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('pagos/reportar');?>" class="nav-link">
										Reportar pago
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="<?php echo base_url('clientes');?>"> 
								Área de Clientes
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<div class="page-container">
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="container">
					<div class="page-content-inner">
						<div class="portlet light portlet-fit ">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-green bold uppercase">
										<?php echo $titulo;?>
									</span>
								</div>
							</div>
							<div class="portlet-body">