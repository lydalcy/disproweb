<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $titulo;?></title>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/simple-line-icons/css/simple-line-icons.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/layout.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/yellow-orange.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/components.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/pricing.min.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/invoice-2.min.css');?>">
</head>
<body class="page-container-bg-solid page-boxed">
	<div class="page-header">
		<div class="page-header-top">
			<div class="container">
				<div class="page-logo">
					<a href="<?php echo base_url('pagos/listado');?>">
						<img src="<?php echo base_url('assets/img/logo.png');?>" alt="logo" class="logo-default" width="120px">
					</a>
				</div>
				<a href="javascript:;" class="menu-toggler"></a>
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<?php
						if ($this->session->userdata('id')) {
							?>
							<li>
								<a href="<?php echo base_url('usuarios/logout');?>">
									<i class="icon-logout"></i> Salir
								</a>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
		</div>
		<div class="page-header-menu">
			<div class="container">
				<nav class="hor-menu">
					<ul class="nav navbar-nav">
						<?php
						if ($this->session->userdata('id')) {
							?>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Pagos
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<li>
										<a href="<?php echo base_url('pagos/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Pedidos
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<li>
										<a href="<?php echo base_url('pedidos/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Hostings
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<?php
									if ($this->session->userdata('nivel') == 1) {
										?>
										<li>
											<a href="<?php echo base_url('hostings/registro');?>" class="nav-link">
												Nuevo
											</a>
										</li>
										<?php
									}
									?>
									<li>
										<a href="<?php echo base_url('hostings/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Dominios
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<?php
									if ($this->session->userdata('nivel') == 1) {
										?>
										<li>
											<a href="<?php echo base_url('dominios/registro');?>" class="nav-link">
												Nuevo
											</a>
										</li>
										<?php
									}
									?>
									<li>
										<a href="<?php echo base_url('dominios/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Clientes
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<li>
										<a href="<?php echo base_url('clientes/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<?php
							if ($this->session->userdata('nivel') == 1) {
								?>
								<li class="menu-dropdown classic-menu-dropdown ">
									<a href="javascript:;"> Usuarios
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu pull-left">
										<li>
											<a href="<?php echo base_url('usuarios/registro');?>" class="nav-link">
												Nuevo
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('usuarios/listado');?>" class="nav-link">
												Listado
											</a>
										</li>
									</ul>
								</li>
								<?php
							}
							?>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Promociones
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<?php
									if ($this->session->userdata('nivel') == 1) {
										?>
										<li>
											<a href="<?php echo base_url('promociones/registro');?>" class="nav-link">
												Nueva
											</a>
										</li>
										<?php
									}
									?>
									<li>
										<a href="<?php echo base_url('promociones/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Bancos
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<?php
									if ($this->session->userdata('nivel') == 1) {
										?>
										<li>
											<a href="<?php echo base_url('bancos/registro');?>" class="nav-link">
												Nuevo
											</a>
										</li>
										<?php
									}
									?>
									<li>
										<a href="<?php echo base_url('bancos/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<?php
							if ($this->session->userdata('nivel') == 1) {
								?>
								<li>
									<a href="<?php echo base_url('iva');?>"> 
										IVA
									</a>
								</li>
								<?php
							}
							?>
							<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Facturas
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-left">
									<li>
										<a href="<?php echo base_url('facturas/listado');?>" class="nav-link">
											Listado
										</a>
									</li>
								</ul>
							</li>
							<?php
							if ($this->session->userdata('nivel') == 1) {
								?>
								<li class="menu-dropdown classic-menu-dropdown ">
									<a href="javascript:;"> Reportes
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu pull-left">
										<li>
											<a href="<?php echo base_url('reportes/planes');?>" class="nav-link" target="_blank">
												Planes de hosting
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/dominios');?>" class="nav-link" target="_blank">
												Dominios
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/dominiosFecha');?>" class="nav-link" target="_blank">
												Dominios por fecha
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/clientes');?>" class="nav-link" target="_blank">
												Clientes
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/usuarios');?>" class="nav-link" target="_blank">
												Usuarios
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/promociones');?>" class="nav-link" target="_blank">
												Promociones
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/bancos');?>" class="nav-link" target="_blank">
												Bancos
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/facturas/1');?>" class="nav-link" target="_blank">
												Facturas Pendientes
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/facturas/2');?>" class="nav-link" target="_blank">
												Facturas Pagadas
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/facturas');?>" class="nav-link" target="_blank">
												Todas las facturas
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/facturasFecha');?>" class="nav-link" target="_blank">
												Facturas por fecha
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/pagosFecha');?>" class="nav-link" target="_blank">
												Pagos por fecha
											</a>
										</li>
										<li>
											<a href="<?php echo base_url('reportes/pedidos');?>" class="nav-link" target="_blank">
												Pedidos
											</a>
										</li>
									</ul>
								</li>
								<li class="menu-dropdown classic-menu-dropdown ">
								<a href="javascript:;"> Mantenimiento
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu pull-left">
										<li>
											<a href="<?php echo base_url('mantenimiento/respaldar');?>" class="nav-link">
												Respaldar
											</a>
										</li>
									</ul>
								</li>
								<?php
							}

						}
						?>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<div class="page-container">
		<div class="page-content-wrapper">
			<div class="page-content">
				<div class="container">
					<div class="page-content-inner">
						<div class="portlet light portlet-fit ">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-green bold uppercase">
										<?php echo $titulo;?>
									</span>
								</div>
							</div>
							<div class="portlet-body">