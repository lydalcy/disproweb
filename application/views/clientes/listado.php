<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-condensed table-striped datatables-clientes">
	<thead>
		<tr>
			<th>Cédula</th>
			<th>Usuario</th>
			<th>Correo</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Direccion</th>
			<th>Teléfono</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($clientes as $cliente) {
			?>
			<tr>
				<td>
					<?php echo $cliente->cedula;?>
				</td>
				<td>
					<?php echo $cliente->usuario;?>
				</td>
				<td>
					<?php echo $cliente->correo;?>
				</td>
				<td>
					<?php echo $cliente->nombre;?>
				</td>
				<td>
					<?php echo $cliente->apellido;?>
				</td>
				<td>
					<?php echo $cliente->direccion;?>
				</td>
				<td>
					<?php echo $cliente->telefono;?>
				</td>
				<td>
					<a href="<?php echo base_url('clientes/editar/' . $cliente->id);?>" data-tooltip="tooltip" data-placement="top" title="Editar">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>