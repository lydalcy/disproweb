<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<form action="<?php echo base_url('clientes/modificar');?>" method="post">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="usuario">Cédula</label>
				<input type="text" name="cedula" id="cedula" class="form-control" value="<?php echo $cliente->cedula;?>" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" class="form-control" readonly value="<?php echo $cliente->usuario;?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required value="<?php echo $cliente->nombre;?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="apellido">Apellido</label>
				<input type="text" name="apellido" id="apellido" class="form-control" required value="<?php echo $cliente->apellido;?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="correo">Correo</label>
				<input type="text" name="correo" id="correo" class="form-control" required value="<?php echo $cliente->correo;?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="telefono">Teléfono</label>
				<input type="text" name="telefono" id="telefono" class="form-control" required value="<?php echo $cliente->telefono;?>">
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="direccion">Dirección</label>
				<textarea name="direccion" id="direccion" cols="30" rows="5" class="form-control" required><?php echo $cliente->direccion;?></textarea>
			</div>
		</div>
		<div class="col-md-12">
			<input type="hidden" name="id" value="<?php echo $cliente->id;?>">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-refresh"></span>
				Modificar
			</button>
		</div>
	</div>
</form>