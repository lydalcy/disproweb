<div class="row">
	<?php
	foreach ($bancos as $banco) {
		?>
		<div class="col-md-3">
			<p>
				<strong>Banco:</strong> <?php echo $banco->nombre;?>
				<br>
				<strong>Cuenta:</strong> <?php echo $banco->nro_cuenta;?>
				<br>
				<strong>Razón social:</strong> <?php echo $banco->razon_social;?>
				<br>
				<strong>RIF:</strong> <?php echo $banco->rif;?>
			</p>
		</div>
		<?php
	}
	?>
</div>
<hr>
<div class="row">
	<?php
	if ($this->session->userdata('cliente_id')) {
		if ($this->session->flashdata('error')) {
			echo $this->session->flashdata('error');
		}
		?>
		<form action="<?php echo base_url('pagos/guardar');?>" method="post">
			<div class="col-md-5">
				<h3>Datos del pago</h3>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="banco">Banco a donde realizó el pago</label>
							<select name="banco" id="banco" class="form-control" required>
								<option value="">Seleccione...</option>
								<?php
								foreach ($bancos as $banco) {
									?>
									<option value="<?php echo $banco->id;?>">
										<?php echo $banco->nombre . ' ('. $banco->nro_cuenta .')';?>
									</option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="referencia">Referencia de la transacción</label>
							<input type="text" name="referencia" id="referencia" class="form-control" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="monto">Monto pagado</label>
							<input type="text" name="monto" id="monto" class="form-control" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="fecha">Fecha en que realizó el pago</label>
							<input type="date" name="fecha" id="fecha" class="form-control" required max="<?php echo date('Y-m-d');?>">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="comentario">Comentario</label>
							<textarea name="comentario" id="comentario" cols="30" rows="3" class="form-control"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<h2>Facturas pendientes</h2>
				<p>
					Seleccione la (s) factura (s) a pagar
				</p>
				<table class="table table-bordered table-hover table-striped table-condensed">
					<thead>
						<tr>
							<th></th>
							<th>Número</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th class="text-right">Subtotal</th>
							<th class="text-right">Desc.</th>
							<th class="text-right">IVA</th>
							<th class="text-right">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($facturas as $factura) {
							?>
							<tr>
								<td>
									<input type="checkbox" name="factura[]" id="<?php echo $factura->id;?>" value="<?php echo $factura->id;?>">
								</td>
								<td><?php echo $factura->id;?></td>
								<td>
									<?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?>
								</td>
								<td>Pendiente</td>
								<td class="text-right">
									<?php
									$subtotal = $totales_facturas[$factura->id]['subtotal'];
									?>
									Bs. <?php echo number_format($subtotal, 2, ',', '.');?>
								</td>
								<td class="text-right">
									Bs. <?php echo number_format($factura->descuento, 2, ',', '.');?>
								</td>
								<td class="text-right">
									<?php
									$total_iva = $totales_facturas[$factura->id]['iva'];
									?>
									Bs. <?php echo number_format($total_iva, 2, ',', '.');?>
								</td>
								<td class="text-right">
									Bs. <?php echo number_format($subtotal - $factura->descuento + $total_iva, 2, ',', '.');?>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
				<button class="btn btn-primary btn-lg" type="submit">
					Notificar pago
				</button>
			</div>
		</form>
		<?php
	} else {
		?>
		<div class="col-md-12">
			<div class="alert alert-success">
				<strong>¡Atención!</strong> Para realizar un pago debe <a href="<?php echo base_url('clientes');?>" class="btn btn-default">Inicar sesión</a>
			</div>
		</div>
		<?php
	}
	?>
</div>