<table class="table table-bordered table-hover table-striped table-condensed datatables-pagos" border="1">
	<thead>
		<tr>
			<th>Referencia</th>
			<th>Fecha</th>
			<th>Comentario</th>
			<th>Banco</th>
			<th>Facturas pagadas</th>
			<th class="derecha">Monto</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($pagos as $pago) {
			?>
			<tr>
				<td>
					<?php echo $pago->referencia_trans;?>
				</td>
				<td>
					<?php echo date('d-m-Y', strtotime($pago->fecha));?>
				</td>
				<td>
					<?php echo $pago->comentario;?>
				</td>
				<td>
					<?php echo $pago->nombre . ' (' . $pago->nro_cuenta . ')';?>
				</td>
				<td>
					<ul>
						<?php
						foreach ($facturas[$pago->id] as $factura) {
							?>
							<li>
								#<?php echo $factura->id;?>
							</li>
							<?php
						}
						?>
					</ul>
				</td>
				<td class="derecha">
					Bs. <?php echo number_format($pago->monto_pagado, 2, ',', '.');?>
				</td>
				<td>
					<?php
					if ($pago->estado_pago == 1) {
						?>
						Pendiente
						<?php
					}
					if ($pago->estado_pago == 2) {
						?>
						Aprobado
						<?php
					}
					if ($pago->estado_pago == 3) {
						?>
						Rechazado
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>