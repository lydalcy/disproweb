<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form action="<?php echo base_url('reportes/pagosPorFecha');?>" method="post">
			<table class="table table-bordered table-condensed">
				<tr>
					<td>Fecha inicial</td>
					<td>
						<input type="date" name="fecha_inicial" max="<?php echo date('Y-m-d');?>" required>
					</td>
				</tr>
				<tr>
					<td>Fecha final</td>
					<td>
						<input type="date" name="fecha_final" max="<?php echo date('Y-m-d');?>" required>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="btn btn-primary">
							Generar reporte
						</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>