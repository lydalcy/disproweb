<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-hover table-striped table-condensed datatables-hostings">
	<thead>
		<tr>
			<th>Nombre</th>
			<th class="text-right">Espacio</th>
			<th class="text-right">Ancho de banda</th>
			<th class="text-right">Precio mensual</th>
			<th class="text-right">Precio trimestral</th>
			<th class="text-right">Precio semestral</th>
			<th class="text-right">Precio anual</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($hostings as $hosting) {
			?>
			<tr>
				<td>
					<?php echo $hosting->nombre;?>
				</td>
				<td class="text-right">
					<?php echo $hosting->espacio;?>
				</td>
				<td class="text-right">
					<?php echo $hosting->banda_ancha;?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($hosting->precio_mensual, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($hosting->precio_trimestral, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($hosting->precio_semestral, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($hosting->precio_anual, 2, ',', '.');?>
				</td>
				<td>
					<?php
					if ($this->session->userdata('nivel') == 1) {
						?>
						<a href="<?php echo base_url('hostings/editar/' . $hosting->id);?>" data-tooltip="tooltip" title="Editar">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>
						<a href="#modal-confirmacion" data-route="<?php echo base_url('hostings/eliminar/' . $hosting->id);?>" data-tooltip="tooltip" data-toggle="modal" title="Eliminar">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>