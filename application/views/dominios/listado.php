<?php
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<table class="table table-bordered table-condensed table-striped datatables-dominios">
	<thead>
		<tr>
			<th>Extensión</th>
			<th class="text-right">Precio de registro</th>
			<th class="text-right">Precio de transferencia</th>
			<th class="text-right">Precio propio</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($dominios as $dominio) {
			?>
			<tr>
				<td>
					<?php echo $dominio->extension;?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($dominio->precio_registro, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($dominio->precio_transferencia, 2, ',', '.');?>
				</td>
				<td class="text-right">
					Bs. <?php echo number_format($dominio->precio_propio, 2, ',', '.');?>
				</td>
				<td>
					<?php
					if ($this->session->userdata('nivel') == 1) {
						?>
						<a href="<?php echo base_url('dominios/editar/' . $dominio->id);?>" data-tooltip="tooltip" data-placement="top" title="Editar">
							<span class="glyphicon glyphicon-pencil"></span>
						</a>
						<a href="#modal-confirmacion" data-route="<?php echo base_url('dominios/eliminar/' . $dominio->id);?>" data-tooltip="tooltip" data-toggle="modal" data-placement="top" title="Eliminar">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
						<?php
					}
					?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>