<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
if ($this->session->flashdata('mensaje')) {
	?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('mensaje');?>
	</div>
	<?php
}
?>
<form action="<?php echo base_url('dominios/modificar');?>" method="post">
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label for="extension">Extensión</label>
				<input type="text" name="extension" id="extension" class="form-control" required value="<?php echo $dominio->extension;?>">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_registro">Precio de registro</label>
				<input type="text" name="precio_registro" id="precio_registro" class="form-control" required value="<?php echo $dominio->precio_registro;?>">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_transferencia">Precio de transferencia</label>
				<input type="text" name="precio_transferencia" id="precio_transferencia" class="form-control" required value="<?php echo $dominio->precio_transferencia;?>">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label for="precio_propio">Precio de propio</label>
				<input type="text" name="precio_propio" id="precio_propio" class="form-control" required value="<?php echo $dominio->precio_propio;?>">
			</div>
		</div>
		<div class="col-md-12">
			<input type="hidden" name="id"  value="<?php echo $dominio->id;?>">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-refresh"></span>
				Modificar
			</button>
		</div>
	</div>
</form>