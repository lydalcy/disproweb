<table class="table table-bordered table-condensed table-striped" border="1">
	<thead>
		<tr>
			<th>Dominio</th>
			<th>Fecha de expiración</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($dominios as $dominio) {
			?>
			<tr>
				<td>
					<?php echo $dominio->dominio;?>
				</td>
				<td>
					Bs. <?php echo date('d-m-Y', strtotime($dominio->fecha_expiracion));?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>