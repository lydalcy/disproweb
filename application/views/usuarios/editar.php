<div class="col-md-10 col-md-offset-1">
	<?php
	if ($this->session->flashdata('error')) {
		echo $this->session->flashdata('error');
	}
	if ($this->session->flashdata('mensaje')) {
		?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $this->session->flashdata('mensaje');?>
		</div>
		<?php
	}
	?>
	<form action="<?php echo base_url('usuarios/modificar');?>" method="post" class="well">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="usuario">Usuario</label>
					<input type="text" name="usuario" id="usuario" class="form-control" readonly value="<?php echo $usuario->usuario;?>">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="password">Contraseña</label>
					<input type="password" name="password" id="password" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="nivel">Nivel de acceso</label>
					<select name="nivel" id="nivel" class="form-control">
						<?php
						foreach ($niveles as $nivel) {
							if ($nivel->id === $usuario->nivel_id) {
								$selected = 'selected';

							} else {
								$selected = null;
							}
							?>
							<option value="<?php echo $nivel->id;?>" <?php echo $selected;?>>
								<?php echo $nivel->nombre;?>
							</option>
							<?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="nombre">Nombre</label>
					<input type="text" name="nombre" id="nombre" class="form-control" required value="<?php echo $usuario->nombre;?>">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="apellido">Apellido</label>
					<input type="text" name="apellido" id="apellido" class="form-control" required value="<?php echo $usuario->apellido;?>">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="correo">Correo</label>
					<input type="text" name="correo" id="correo" class="form-control" required value="<?php echo $usuario->correo;?>">
				</div>
			</div>
			<div class="col-md-12">
				<input type="hidden" name="id" value="<?php echo $usuario->id;?>">
				<button class="btn btn-primary" type="submit">
					<span class="glyphicon glyphicon-refresh"></span>
					Modificar
				</button>
			</div>
		</div>
	</form>
</div>