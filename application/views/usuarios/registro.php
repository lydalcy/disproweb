<?php
if ($this->session->flashdata('error')) {
	echo $this->session->flashdata('error');
}
?>
<form action="<?php echo base_url('usuarios/guardar');?>" method="post">
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" class="form-control" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="password">Contraseña</label>
				<input type="password" name="password" id="password" class="form-control" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="nivel">Nivel de acceso</label>
				<select name="nivel" id="nivel" class="form-control">
					<option value="">Seleccione...</option>
					<?php
					foreach ($niveles as $nivel) {
						?>
						<option value="<?php echo $nivel->id;?>">
							<?php echo $nivel->nombre;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="apellido">Apellido</label>
				<input type="text" name="apellido" id="apellido" class="form-control" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="correo">Correo</label>
				<input type="text" name="correo" id="correo" class="form-control" required>
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn btn-primary" type="submit">
				<span class="glyphicon glyphicon-ok"></span>
				Registrar
			</button>
		</div>
	</div>
</form>