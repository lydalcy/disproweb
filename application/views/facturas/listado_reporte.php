<table class="table table-bordered table-condensed table-striped datatables-facturas" border="1">
	<thead>
		<tr>
			<th>Número</th>
			<th>Cliente</th>
			<th>Fecha</th>
			<th>Estado</th>
			<th>Subtotal</th>
			<th>Descuento</th>
			<th>IVA</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$subtotal = 0;
		$iva = 0;
		$descuento = 0;
		$total = 0;
		foreach ($facturas as $factura) {
			?>
			<tr>
				<td><?php echo $factura->id;?></td>
				<td>
					<b>C.I.</b> <?php echo $factura->cedula;?>
					<br>
					<b>Nombre:</b> <?php echo $factura->nombre;?>
					<br>
					<b>Apellido:</b> <?php echo $factura->apellido;?>
					<br>
					<b>Correo:</b> <?php echo $factura->correo;?>
				</td>
				<td>
					<?php echo date('d-m-Y', strtotime($factura->fecha_creacion));?>
				</td>
				<td>
					<?php
					switch ($factura->estado_id) {
						case '1': {
							echo 'Pendiente';
							break;
						}
						case '2': {
							echo 'Pagada';
							break;
						}
						case '3': {
							echo 'Cancelada';
							break;
						}
					}
					?>
				</td>
				<td class="text-right">
					<?php
					$subtotal_factura = $totales_facturas[$factura->id]['subtotal'];
					$subtotal += $subtotal_factura;
					?>
					Bs. <?php echo number_format($subtotal_factura, 2, ',', '.');?>
				</td>
				<td class="text-right">
					<?php
					$descuento += $factura->descuento;
					?>
					Bs. <?php echo number_format($factura->descuento, 2, '.', ',');?>
				</td>
				<td class="text-right">
					<?php
					$total_iva = $totales_facturas[$factura->id]['iva'];
					$iva += $total_iva;
					?>
					Bs. <?php echo number_format($total_iva, 2, '.', ',');?>
				</td>
				<td class="text-right">
					<?php
					$total += $subtotal - $factura->descuento + $total_iva;
					?>
					Bs. <?php echo number_format($subtotal - $factura->descuento + $total_iva, 2, '.', ',');?>
				</td>
			</tr>
			<?php
		}
		if ($this->uri->segment(3) == 2) {
			?>
			<tr>
				<td colspan="4" class="derecha">
					Totales
				</td>
				<td>
					Bs. <?php echo number_format($subtotal, 2, ',', '.');?>
				</td>
				<td>
					Bs. <?php echo number_format($descuento, 2, ',', '.');?>
				</td>
				<td>
					Bs. <?php echo number_format($iva, 2, ',', '.');?>
				</td>
				<td>
					Bs. <?php echo number_format($total, 2, ',', '.');?>
				</td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>