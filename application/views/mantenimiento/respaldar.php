<?php
error_reporting(0);
$conexion;
$servidor = "localhost";
$usuario = "root";
$password = "";
$base_datos = "disproweb";

$idioma="assets/backup/lang_spanish.php";		// Idioma
$mwBack="mwBackup ver.1.2";		// Aparece en la cabecera del archivo backup.
$nombre = "respaldo.sql";			// Nombre del fichero que se generará. 
$descarga= false;				// Si el archivo generado se ofrece para la descarga.       
$drop = true;					// Determina si la tabla será vaciada (si existe) cuando  restauremos la tabla. 
$path ="./assets/respaldos";				// path a donde van las copias (relativa al script).
		
$conexion = mysql_pconnect($servidor,$usuario,$password);
mysql_query("SET NAMES 'utf8'"); //Permite almacenar acentos y Ñ
mysql_select_db($base_datos,$conexion);

if( !@function_exists( 'gzopen' ) ) {
	$hayZlib = false;
	$compresion = ""; 
}
else {
	$hayZlib = true;
	$compresion = "gz"; 
}
$parent="../index.php";
?>
<HTML>
	<HEAD>
		<TITLE> <?php echo _TITULO_respaldar_BD;?></TITLE>
		<link href="<?php echo base_url('assets/backup/backup.css');?>" rel="stylesheet" type="text/css">
	</HEAD>
	<body>
		<center>
			<?php
			if (!isset($_POST["zip"])){
				?>
				<FORM name="miForm" METHOD=POST ACTION="<?php echo base_url('mantenimiento/respaldar');?>">
					<SCRIPT LANGUAGE="JavaScript">
						<!--	
						function procede() {
							miForm.submit();
						}
						function cancela() {
							var ventanatop=top.location.href;
							if (ventanatop.indexOf ("respaldar_BD")==-1)
							{
					// No es un venta, es un frame
					window.parent.location.href="<?php echo $parent;?>";
				}
				else {
					window.opener.location.href="<?php echo $parent;?>";
					window.close();
				}
			}

			function pulsarTecla() {
				var tecla = event.keyCode;
				var teclaReal = String.fromCharCode(tecla);

				if (tecla==13) {
					tecla=9;
					return event.keyCode=tecla;
				}
			}
			function Tecla() {
				var tecla = event.keyCode;
				var teclaReal = String.fromCharCode(tecla);

				if (tecla==27) {

					var ventanatop=top.location.href;
					if (ventanatop.indexOf ("mwRestore")==-1)
					{
						// No es un venta, es un frame
						window.parent.location.href="<?php echo $parent;?>";

					}
					else {
						window.opener.location.href="<?php echo $parent;?>";
						window.close();
					}
				}
			}

			document.onkeydown = pulsarTecla;
			document.onkeyup = Tecla;
		//-->
	</SCRIPT>
	<br>
	<div id="cuadro">
		<div class="titulos"><span class="titulo">Se realizar&aacute; un RESPALDO de la Base de Datos</span></div>
		<TABLE class="tabla2">
			<TR>
				<TD class="standard"><FIELDSET>
					<LEGEND>&nbsp;Compresi&oacute;n</LEGEND>
					<TABLE>
						<TR>
							<TD><INPUT TYPE="radio" NAME="zip" value="si" checked>
								Comprimido</TD>
								<!-- Por defecto est&szlig; activado el valor SI -->
								<TD><INPUT TYPE="radio" NAME="zip" value="no" >
									Sin comprimir</TD>
								</TR>
							</TABLE>
						</FIELDSET></TD>
					</TR>
				</TABLE><br>
				<BUTTON name="ok" value="ok" onclick="procede()">Respaldar</BUTTON>&nbsp;&nbsp;
				<BUTTON name="cancelar" value="cancelar" onclick="cancela()">Cancelar</BUTTON>
				<br>
				<br>
			</div>
		</FORM>
		<?php
	}
	else {
		// 
		//
		// 2ª Parte.
		//
		//
		// mostramos la barra y textos
		//
		?>
		<br>
		<div id="cuadro">
			<div class="titulos">Base de Datos Respaldada</div>
			<br>
			<TABLE class="tabla3">
				<TR>
					<TD class="standard" colspan="2" width='352' align="left" style='font-size:14px;'>Base de Datos: <B><FONT SIZE="" COLOR="#7B91B4"><?php echo $base_datos;?></FONT></B></TD>
				</TR>
				<TR>
					<TD class="standard" width='50' style='font-size:14px;' >Estado: </td>
						<TD class="standard" width='302' align="left" style='font-size:14px;'><B><div id='texto' style='position:relative;color: #7B91B4;'></div></B></TD>
					</TR>
					<TR>
						<TD class="standard" colspan="2" align="center"><br>
							<TABLE  cellspacing="1" cellpadding="1" width='302' bgcolor="#7B91B4">
								<TR>
									<TD bgcolor="#ffffff" align="left" style='font-size:14px;' VALIGN="middle"><div id='progreso' style='position:relative;overflow:hidden'><div id='porcentaje' style='position:absolute;left:130;'>0 %</div><IMG SRC="<?php echo base_url('assets/backup/trans.gif');?>" WIDTH="1" HEIGHT="15" BORDER=0 ALT=""></div></TD>
								</TR>
							</TABLE><br>
						</TD>
					</TR>
				</TABLE>
				<br>
				<br>
			</div>
			<?php
		//
		// Se procede a la creación del backup
		//

		// 
		// muestra un texto arriba de la barra de progreso
		//
			function msg_texto($texto) {
				ob_start(); 
				?>
				<SCRIPT LANGUAGE="JavaScript">
					<!--
					document.getElementById('texto').innerHTML= "<?php echo $texto;?>";
			//-->
		</SCRIPT>
		<?php
		flush(); 
		ob_end_flush(); 
	}

		//
		// muestra la barra de progreso
		//
	function procesa_barra($porcentaje,$pixeles) {
		ob_start(); 
		?>
		<SCRIPT LANGUAGE="JavaScript">
			<!--
			document.getElementById('progreso').innerHTML="<div id='porcentaje' style='position:absolute;left:130;'><B><?php echo $porcentaje.' %';?></B></div><IMG SRC='<?php echo base_url('assets/backup/progreso.gif');?>' WIDTH='<?php echo $pixeles;?>'  HEIGHT='15' BORDER=0>";
			//-->
		</SCRIPT>
		<?php
		flush(); 
		ob_end_flush(); 
	}



		$error="no";	// errores de sql

		//
		// establecemos conexión
		// 
		if ($conexion = mysql_connect($servidor, $usuario, $password) ) {
			msg_texto(_MENSAJE_PROCESANDO." . . .") ;
			// conexión establecida
		}
		else { 
			msg_texto(_MENSAJE_NO_CONECTA.mysql_error()) ;
			$error="si";
			// error al conectar
		}
		if ($error=="no") {
			if (!mysql_select_db($base_datos, $conexion) ) {
				msg_texto(_MENSAJE_NO_SELECCIONA.mysql_error()) ;
				$error="si";
				// error al seleccionar base de datos
			}
		}
		if ($error=="no") {
			//
			// busqueda de tablas para backup
			//
			$sql = "SHOW TABLE STATUS;"; 
			if ($respuesta = mysql_query($sql, $conexion)) {
				msg_texto(_MENSAJE_CONECTANDO." . . .") ;
			}
			else {
				msg_texto(_MENSAJE_NO_CONSULTA.mysql_error()) ;
				$error="si";
			}
			while ($fila = mysql_fetch_array($respuesta)) { 
				$tablas[0] = "niveles"; 
				$tablas[1] = "bitacora"; 
				$tablas[2] = "usuarios"; 
				$tablas[3] = "estados_facturas"; 
				$tablas[4] = "iva";
				$tablas[5] = "bancos"; 
				$tablas[6] = "estados_pagos";
				$tablas[7] = "pagos"; 
				$tablas[8] = "factura"; 
				$tablas[9] = "detalle_factura"; 
				$tablas[10] = "dominios"; 
				$tablas[11] = "promociones"; 
				$tablas[12] = "clientes"; 
				$tablas[13] = "pedidos"; 
				$tablas[14] = "dominio_pedido"; 
				$tablas[15] = "planes"; 
				$tablas[16] = "planes_pedidos"; 
				$tamano[] = $fila['Rows']; 
			}	
		}

		if ($error=="no") {

			//
			// cabecera del archivo backup
			//
			// $info es un array con los datos del servidor
			//
			ob_start(); 
			print_r($tablas); 
			$representacion = ob_get_contents(); 
			ob_end_clean (); 
			preg_match_all('/(\[\d+\] => .*)\n/', $representacion, $matches); 
			$info['tablas'] = implode(";  ", $matches[1]); 

			//
			// $dump es la variable que contiene lo que vamos a respaldar
			//
			$dump = "# ===================================================================\n";
			$dump .="#  ".$mwBack."\n";
			$dump .="#  Copias de seguridad\n";
			$dump .="#  \n";
			$dump .="#  Servidor: {$_SERVER['HTTP_HOST']} \n";
			$dump .="#  Base de datos: `".$base_datos."` \n";
			$dump .="#  \n";
			$dump .="# -------------------------------------------------------------------\n"; 
			$dump .="# TABLAS:\n"; 
			$dump .="#  ".count($tablas)."\n";
			//
			// calculamos los registros que hay
			//
			$registros=0;
			for ($n=0;$n<count($tablas);$n++) {
				$registros+=$tamano[$n];
			}
			$dump .="# -------------------------------------------------------------------\n"; 
			$dump .= "#\n";
			$dump .= "#Se borra la base de datos y se vuelve a crear\n";  
			$dump .= "#\n"; 
			$dump .= "DROP DATABASE IF EXISTS `$base_datos`; \n";
			$dump .= "CREATE DATABASE `$base_datos`; \n";
			$dump .= "USE `$base_datos`; \n";

			//
			// $procesados lleva el control del numero de insert procesados
			// 
			$procesados=0;
			//
			// $ultimo_proceso lleva el control de la llamada a la barra de progreso
			//
			$ultimo_proceso=0;

			//
			// Si está disponible Zlib
			//
			if ( ($hayZlib) && ($zip=="si")) {
				$compresion ="gz";
			}
			else {
				$compresion ="";
			}

			// 
			// se empieza la escritura del backup
			//
			if( $compresion =="gz") {
				$nombre.=".gz";
				$filehandle = gzopen( $path."/".$nombre, 'w' );
				gzwrite( $filehandle, $dump );
			}
			else {
				$filehandle = fopen( $path."/".$nombre, 'w' );
				fwrite( $filehandle, $dump );
			}

			//
			// recorremos las tablas
			//
			msg_texto(_MENSAJE_CREANDO." . . .") ;
			
			
			
			foreach ($tablas as $tabla) { 
				// $create_table_query -> sentencia de create
				//
				$create_table_query = ""; 
				// 
				// $insert_table_query -> sentencias de insert
				//
				$insert_into_query = ""; 
				//
				// estructura de la tabla
				//
				$sql = "SHOW CREATE TABLE $tabla;"; 
				if (!($respuesta = mysql_query($sql, $conexion))) {
					msg_texto("No se pudo ejecutar la consulta: ".mysql_error()); 
					$error="si";
					exit();
				}
				while ($fila = mysql_fetch_array($respuesta, MYSQL_NUM)) { 
					$create_table_query = $fila[1].";"; 
				} 

				$sql = "SELECT * FROM $tabla;"; 
				if (!($respuesta = mysql_query($sql, $conexion))) {
					msg_texto("No se pudo ejecutar la consulta: ".mysql_error()); 
					$error="si";
					exit();
				}

				$dump = "#\n"; 
				$dump .= "#\n"; 
				$dump .= "#  Estructura de la tabla `$tabla` \n";
				$dump .= "# ------------------------------------- \n";
				$dump .= "#\n"; 
				$dump .= "#\n"; 
				$dump .= "$create_table_query\n";
				$dump .= "#\n"; 
				$dump .= "#\n"; 
				$dump .= "#  Carga de datos de la tabla `$tabla` \n";
				$dump .= "# ------------------------------------- \n";
				$dump .= "#\n"; 
				$dump .= "#\n"; 

				//
				// escribimos el vaciado y estructura de la tabla
				//
				if( $compresion =="gz") {
					gzwrite( $filehandle, $dump );
				}
				else {
					fwrite( $filehandle, $dump );
				}
				//
				// cada registro
				//
				while ($fila = mysql_fetch_array($respuesta, MYSQL_ASSOC)) { 
					//
					// recogemos los valores de cada regisrtro para los insert
					//
					$columnas = array_keys($fila); 
					foreach ($columnas as $columna) { 

						if ( gettype($fila[$columna]) == "NULL" ) { 
							$values[] = "NULL"; 
						} else { 
							if (!(is_string ($fila[$columna]))) { 
								$values[] = $fila[$columna]; 
							}
							else {
								$values[] = "'". mysql_real_escape_string($fila[$columna])."'"; 
							}
						} 
					} 
					$insert_into_query = "INSERT INTO `$tabla` VALUES (".implode(", ", $values).");\n"; 
					$dump = "$insert_into_query"; 

					//
					// escribimos el insert
					//
					if( $compresion =="gz") {
						gzwrite( $filehandle, $dump );
					}
					else {
						fwrite( $filehandle, $dump );
					}

					unset($values); 
					$procesados++;
					//
					// calculamos según los procesados los pixeles y el porcentaje 
					// a representar en la barra de progreso
					//
					$pixeles= round(($procesados*300)/$registros);
					$porcentaje= round(($procesados*100)/$registros);
					//
					// solo si hay que aumentar de tamaño la barra de progreso
					//
					if ($pixeles>$ultimo_proceso) {
						$ultimo_proceso=$pixeles;
						procesa_barra($porcentaje,$pixeles);
					}
				} 
				//
				// terminada la tabla actual
				//
			} 
			//
			// terminación de recorrer las tablas
			//




			// 
			//
			// Fin 2ª Parte.
			if( $compresion =="gz") {
				gzclose( $filehandle );
			}
			else {
				fclose( $filehandle );
			}
			msg_texto(_MENSAJE_CREADO.": <a href='".base_url().$path."/".$nombre."'>".$nombre."</a>") ;
			?>
			<SCRIPT LANGUAGE="JavaScript">
				<!--	
				var ventanatop=top.location.href;


				function hecho() {
					if (ventanatop.indexOf ("respaldar_BD")==-1)
					{
						// No es un venta, es un frame
						window.parent.location.href="<?php echo $parent;?>";

					}
					else {
						window.opener.location.href="<?php echo $parent;?>";
						window.close();
					}
				}

				function pulsarTecla() {
					var tecla = event.keyCode;
					var teclaReal = String.fromCharCode(tecla);

					if (tecla==13) {
						tecla=9;
						return event.keyCode=tecla;
					}
				}
				function Tecla() {
					var tecla = event.keyCode;
					var teclaReal = String.fromCharCode(tecla);

					if (tecla==27) {

						var ventanatop=top.location.href;
						if (ventanatop.indexOf ("mwRestore")==-1)
						{
							// No es un venta, es un frame
							window.parent.location.href="<?php echo $parent;?>";

						}
						else {
							window.opener.location.href="<?php echo $parent;?>";
							window.close();
						}
					}
				}

				document.onkeydown = pulsarTecla;
				document.onkeyup = Tecla;

			//-->
		</SCRIPT>
		<br>

		<?php
			//
			// Se procede a ofrecer el archivo del backup
			//

		if ($descarga) {
			?>
			<FORM name="miForm" METHOD=POST ACTION="<?php echo base_url('respaldos/index');?>">
				<INPUT TYPE="hidden" name="path" value="<?php echo $path;?>">
					<INPUT TYPE="hidden" name="nombre" value="<?php echo $nombre;?>">
						<INPUT TYPE="hidden" name="zip" value="<?php echo $zip;?>">
						</FORM>
						<SCRIPT LANGUAGE="JavaScript">
							<!--
							miForm.submit();
				//-->
			</SCRIPT>

			<?php

		}
	}
}