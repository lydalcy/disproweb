<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bancos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bancos_model');
	}

	public function editar($id)
	{
		$datos['banco'] = $this->Bancos_model->consultar($id);
		$datos['titulo'] = 'Edición de bancos';
		$datos['contenido'] = 'bancos/editar';
		$this->load->view('administrador', $datos);
	}

	public function eliminar($id)
	{
		$this->Bancos_model->eliminar($id);
		$this->Usuarios_model->bitacora('Bancos', 'Eliminar');
		$this->session->set_flashdata('mensaje', 'Banco eliminado exitosamente');
		redirect('bancos/listado');
	}

	public function guardar()
	{
		if ($this->form_validation->run('registrar-bancos') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('bancos/registro');
			
		} else {
			$datos = array(
				'nombre' => $this->input->post('nombre'),
				'nro_cuenta' => $this->input->post('nro_cuenta'),
				'razon_social' => $this->input->post('razon_social'),
				'rif' => $this->input->post('rif')
				);
			$this->Bancos_model->registrar($datos);
			$this->Usuarios_model->bitacora('Bancos', 'Registrar');
			$this->session->set_flashdata('mensaje', 'Banco registrado exitosamente');
			redirect('bancos/listado');
		}
	}

	public function index()
	{
		$datos['bancos'] = $this->Bancos_model->listado();
		$datos['titulo'] = 'Datos bancarios';
		$datos['contenido'] = 'bancos/listado_cliente';
		$this->load->view('plantilla', $datos);
	}

	public function listado()
	{
		$datos['bancos'] = $this->Bancos_model->listado();
		$datos['titulo'] = 'Lista de bancos';
		$datos['contenido'] = 'bancos/listado';
		$this->load->view('administrador', $datos);
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-bancos') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('bancos/editar/' . $id);
			
		} else {
			$datos = array(
				'nombre' => $this->input->post('nombre'),
				'nro_cuenta' => $this->input->post('nro_cuenta'),
				'razon_social' => $this->input->post('razon_social'),
				'rif' => $this->input->post('rif'),
				);
			$this->Bancos_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('Bancos', 'Modificar');
			$this->session->set_flashdata('mensaje', 'Banco modificado exitosamente');
			redirect('bancos/editar/' . $id);
		}
	}

	public function registro()
	{
		$datos['titulo'] = 'Registro de bancos';
		$datos['contenido'] = 'bancos/registro';
		$this->load->view('administrador', $datos);
	}
}
