<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Niveles_model');
	}

	public function editar($id)
	{
		$datos['niveles'] = $this->Niveles_model->listado();
		$datos['usuario'] = $this->Usuarios_model->consultar($id);
		$datos['titulo'] = 'Edición de usuarios';
		$datos['contenido'] = 'usuarios/editar';
		$this->load->view('administrador', $datos);
	}

	public function eliminar($id)
	{
		$this->Usuarios_model->eliminar($id);
		$this->Usuarios_model->bitacora('Usuarios', 'Eliminar');
		$this->session->set_flashdata('mensaje', 'Usuario eliminado exitosamente');
		redirect('usuarios/listado');
	}

	public function guardar()
	{
		if ($this->form_validation->run('registro-usuarios') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('usuarios/registro');
			
		} else {
			$datos = array(
				'usuario' => $this->input->post('usuario'),
				'password' => md5($this->input->post('password')),
				'nivel_id' => $this->input->post('nivel'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'correo' => $this->input->post('correo'),
				);
			$this->Usuarios_model->registrar($datos);
			$this->Usuarios_model->bitacora('Usuarios', 'Registrar');
			$this->session->set_flashdata('mensaje', 'Usuario registrado exitosamente');
			redirect('usuarios/listado');
		}
	}

	/**
	 * Muestra el login de usuarios
	 * 
	 * @return view Vista de login
	 */
	public function index()
	{
		$datos['titulo'] = 'Iniciar sesión';
		$datos['contenido'] = 'usuarios/login';
		$this->load->view('administrador', $datos);
	}

	public function listado()
	{
		$datos['usuarios'] = $this->Usuarios_model->listado();
		$datos['titulo'] = 'Lista de usuarios';
		$datos['contenido'] = 'usuarios/listado';
		$this->load->view('administrador', $datos);
	}

	public function login()
	{
		$usuario = $this->input->post('usuario');
		$password = md5($this->input->post('password'));

		if ($this->Usuarios_model->login($usuario, $password)) {			
			redirect('pagos/listado');
		} else {
			$this->session->set_flashdata('error', 'Datos incorrectos');
			redirect('usuarios');
		}
	}

	public function logout()
	{
		$this->Usuarios_model->bitacora('Usuarios', 'Cerrar sesión');
		$this->session->sess_destroy();
		redirect('usuarios');
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-usuarios') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('usuarios/editar/' . $id);
			
		} else {
			if ($this->input->post('password')) {
				$datos = array(
					'usuario' => $this->input->post('usuario'),
					'password' => md5($this->input->post('password')),
					'nivel_id' => $this->input->post('nivel'),
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'correo' => $this->input->post('correo'),
					);
			} else {
				$datos = array(
					'usuario' => $this->input->post('usuario'),
					'nivel_id' => $this->input->post('nivel'),
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'correo' => $this->input->post('correo'),
					);
			}
			$this->Usuarios_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('Usuarios', 'Modificar');
			$this->session->set_flashdata('mensaje', 'Usuario modificado exitosamente');
			redirect('usuarios/editar/' . $id);
		}
	}

	public function panel()
	{
		$datos['titulo'] = 'Panel del usuario';
		$datos['contenido'] = 'usuarios/panel';
		$this->load->view('administrador', $datos);
	}

	public function registro()
	{
		$datos['niveles'] = $this->Niveles_model->listado();
		$datos['titulo'] = 'Registro de usuarios';
		$datos['contenido'] = 'usuarios/registro';
		$this->load->view('administrador', $datos);
	}
}
