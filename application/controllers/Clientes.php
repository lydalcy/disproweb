<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Maneja las solicitudes de los clientes
 */
class Clientes extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Clientes_model');
		$this->load->model('Facturas_model');
		$this->load->model('Iva_model');
		$this->load->model('Pagos_model');
		$this->load->model('Pedidos_model');
	}

	public function editar($id)
	{
		$datos['cliente'] = $this->Clientes_model->consultar($id);
		$datos['titulo'] = 'Edición de clientes';
		$datos['contenido'] = 'clientes/editar';
		$this->load->view('administrador', $datos);
	}

	/**
	 * Almacena en base de datos a los clientes
	 * 
	 * @return redirect Registro de usuarios
	 */
	public function guardar()
	{
		if ($this->form_validation->run('registro-clientes') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pedidos/procesar');
			
		} else {
			$datos = array(
				'cedula' => $this->input->post('cedula'),
				'usuario' => $this->input->post('usuario'),
				'password' => md5($this->input->post('password')),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'correo' => $this->input->post('correo'),
				'telefono' => $this->input->post('telefono'),
				'direccion' => $this->input->post('direccion')
				);
			$cliente = $this->Clientes_model->registrar($datos);
			redirect('pedidos/guardar/' . $cliente);
		}
	}

	/**
	 * Muestra el login de clientes
	 * 
	 * @return view Vista de login
	 */
	public function index()
	{
		if (!$this->session->userdata('cliente_id')) {
			$datos['titulo'] = 'Iniciar sesión';
			$datos['contenido'] = 'clientes/login';
			$this->load->view('plantilla', $datos);
		} else {
			$cliente = $this->session->userdata('cliente_id');
			$pagos = $this->Pagos_model->pagosCliente($cliente);
			$facturas = $this->Facturas_model->facturasCliente($cliente, null);
			$iva = $this->Iva_model->consultar();

			$totales_facturas = array();
			foreach ($facturas as $factura) {
				$datos_dominios = $this->Pedidos_model->consultarDetallesDominios($factura->pedido_id);
				
				$total_dominios = 0;
				foreach ($datos_dominios as $dominio) {
					if ($dominio->tipo == 'Registro') {
						$total_dominios += $dominio->precio_registro;
					}
					if ($dominio->tipo == 'Transferencia') {
						$total_dominios += $dominio->precio_transferencia;
					}
					if ($dominio->tipo == 'Propio') {
						$total_dominios += $dominio->precio_propio;
					}
				}

				$datos_planes = $this->Pedidos_model->consultarDetallesPlanes($factura->pedido_id);
				$total_planes = 0;
				foreach ($datos_planes as $plan) {
					if ($plan->ciclo_facturacion == 'Mensual') {
						$total_planes += $plan->precio_mensual;
					}
					if ($plan->ciclo_facturacion == 'Trimestral') {
						$total_planes += $plan->precio_trimestral;
					}
					if ($plan->ciclo_facturacion == 'Semestral') {
						$total_planes += $plan->precio_semestral;
					}
					if ($plan->ciclo_facturacion == 'Anual') {
						$total_planes += $plan->precio_anual;
					}
				}

				$subtotal = $total_dominios + $total_planes;
				$total_iva = ($subtotal - $factura->descuento) * ($iva->iva / 100);
				$data = array(
					'subtotal' => $subtotal,
					'iva' => $total_iva
				);
				$totales_facturas[$factura->id] = $data;
			}

			$facturas_pagadas = array();
			foreach ($pagos as $pago) {
				$facturas_pagadas[$pago->id] = $this->Facturas_model->listadoDetallesFactura($pago->id);
			}
			$datos['facturas'] = $facturas;
			$datos['facturas_pagadas'] = $facturas_pagadas;
			$datos['totales_facturas'] = $totales_facturas;
			$datos['pagos'] = $pagos;
			$datos['titulo'] = 'Area de clientes';
			$datos['contenido'] = 'clientes/panel';
			$this->load->view('plantilla', $datos);
		}
	}

	public function listado()
	{
		$datos['clientes'] = $this->Clientes_model->listado();
		$datos['titulo'] = 'Lista de clientes';
		$datos['contenido'] = 'clientes/listado';
		$this->load->view('administrador', $datos);
	}

	public function login()
	{
		$usuario = $this->input->post('usuario');
		$password = md5($this->input->post('password'));
		$accion = $this->input->post('accion');

		if ($accion == 'pedido') {
			if ($this->Clientes_model->login($usuario, $password)) {			
				$cliente = $this->session->userdata('cliente_id');
				redirect('pedidos/guardar/' . $cliente);
			} else {
				$this->session->set_flashdata('mensaje', 'Datos de usuario incorrectos');
				redirect('pedidos/procesar');
			}
		} else {
			if (!$this->Clientes_model->login($usuario, $password)) {			
				$this->session->set_flashdata('error', 'Datos de usuario incorrectos');
			}
			
			redirect('clientes');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('clientes');
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-clientes') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('clientes/editar/' . $id);
			
		} else {
			$datos = array(
				'cedula' => $this->input->post('cedula'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'correo' => $this->input->post('correo'),
				'telefono' => $this->input->post('telefono'),
				'direccion' => $this->input->post('direccion')
				);
			$this->Clientes_model->modificar($id, $datos);
			$this->session->set_flashdata('mensaje', 'Cliente modificado exitosamente');
			redirect('clientes/editar/' . $id);
		}
	}

	public function panel()
	{
		$datos['titulo'] = 'Panel del cliente';
		$datos['contenido'] = 'clientes/panel';
		$this->load->view('plantilla', $datos);
	}

	/**
	 * Muestra la vista de registro de clientes
	 * 
	 * @return view Vista de registro
	 */
	public function registro()
	{
		$datos['titulo'] = 'Registro de clientes';
		$datos['contenido'] = 'clientes/registro';
		$this->load->view('plantilla', $datos);
	}

}
