<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dominios extends CI_Controller
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dominios_model');
	}

	public function editar($id)
	{
		$datos['dominio'] = $this->Dominios_model->consultar($id);
		$datos['titulo'] = 'Edición de dominio';
		$datos['contenido'] = 'dominios/editar';
		$this->load->view('administrador', $datos);
	}

	public function eliminar($id)
	{
		$this->Dominios_model->eliminar($id);
		$this->Usuarios_model->bitacora('Dominios', 'Eliminar');
		$this->session->set_flashdata('mensaje', 'Dominio eliminado exitosamente');
		redirect('dominios/listado');
	}

	public function guardar()
	{
		if ($this->form_validation->run('registro-dominios') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('dominios/registro');
			
		} else {
			$datos = array(
				'extension' => $this->input->post('extension'),
				'precio_registro' => $this->input->post('precio_registro'),
				'precio_transferencia' => $this->input->post('precio_transferencia'),
				'precio_propio' => $this->input->post('precio_propio'),
				);
			$this->Dominios_model->registrar($datos);
			$this->Usuarios_model->bitacora('Dominios', 'Registrar');
			$this->session->set_flashdata('mensaje', 'Dominio registrado exitosamente');
			redirect('dominios/listado');
		}
	}

	public function listado()
	{
		$datos['dominios'] = $this->Dominios_model->listado();
		$datos['titulo'] = 'Lista de dominios';
		$datos['contenido'] = 'dominios/listado';
		$this->load->view('administrador', $datos);
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-dominios') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('dominios/editar/' . $id);
			
		} else {
			$datos = array(
				'extension' => $this->input->post('extension'),
				'precio_registro' => $this->input->post('precio_registro'),
				'precio_transferencia' => $this->input->post('precio_transferencia'),
				'precio_propio' => $this->input->post('precio_propio'),
				);
			$this->Dominios_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('Dominios', 'Modificar');
			$this->session->set_flashdata('mensaje', 'Dominio modificado exitosamente');
			redirect('dominios/editar/' . $id);
		}
	}

	public function registro()
	{
		$datos['titulo'] = 'Registro de dominios';
		$datos['contenido'] = 'dominios/registro';
		$this->load->view('administrador', $datos);
	}

}
