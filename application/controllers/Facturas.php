<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Facturas_model');
		$this->load->model('Iva_model');
		$this->load->model('Pedidos_model');
	}

	public function listado()
	{
		$facturas = $this->Facturas_model->listado();
		$iva = $this->Iva_model->consultar();
		$totales_facturas = array();
		foreach ($facturas as $factura) {
			$datos_dominios = $this->Pedidos_model->consultarDetallesDominios($factura->pedido_id);

			$total_dominios = 0;
			foreach ($datos_dominios as $dominio) {
				if ($dominio->tipo == 'Registro') {
					$total_dominios += $dominio->precio_registro;
				}
				if ($dominio->tipo == 'Transferencia') {
					$total_dominios += $dominio->precio_transferencia;
				}
				if ($dominio->tipo == 'Propio') {
					$total_dominios += $dominio->precio_propio;
				}
			}

			$datos_planes = $this->Pedidos_model->consultarDetallesPlanes($factura->pedido_id);
			$total_planes = 0;
			foreach ($datos_planes as $plan) {
				if ($plan->ciclo_facturacion == 'Mensual') {
					$total_planes += $plan->precio_mensual;
				}
				if ($plan->ciclo_facturacion == 'Trimestral') {
					$total_planes += $plan->precio_trimestral;
				}
				if ($plan->ciclo_facturacion == 'Semestral') {
					$total_planes += $plan->precio_semestral;
				}
				if ($plan->ciclo_facturacion == 'Anual') {
					$total_planes += $plan->precio_anual;
				}
			}

			$subtotal = $total_dominios + $total_planes;
			$total_iva = ($subtotal - $factura->descuento) * ($iva->iva / 100);
			$data = array(
				'subtotal' => $subtotal,
				'iva' => $total_iva
				);
			$totales_facturas[$factura->id] = $data;
		}

		$datos['facturas'] = $facturas;
		$datos['totales_facturas'] = $totales_facturas;
		$datos['titulo'] = 'Lista de facturas';
		$datos['contenido'] = 'facturas/listado';
		$this->load->view('administrador', $datos);
	}
}
