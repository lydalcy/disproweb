<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promociones extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Promociones_model');
		$this->load->helper('string');
	}

	public function editar($id)
	{
		$datos['promocion'] = $this->Promociones_model->consultar($id);
		$datos['titulo'] = 'Edición de promoción';
		$datos['contenido'] = 'promociones/editar';
		$this->load->view('administrador', $datos);
	}

	public function eliminar($id)
	{
		$this->Promociones_model->eliminar($id);
		$this->Usuarios_model->bitacora('Promociones', 'Eliminar');
		$this->session->set_flashdata('mensaje', 'Promoción eliminada exitosamente');
		redirect('promociones/listado');
	}

	public function guardar()
	{
		if ($this->form_validation->run('registro-promociones') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('promociones/registro');
			
		} else {
			$datos = array(
				'codigo_cupon' => random_string('alnum', 16),
				'desc_promo' => $this->input->post('descuento'),
				'fecha_valida' => $this->input->post('valido_hasta'),
				);
			$this->Promociones_model->registrar($datos);
			$this->Usuarios_model->bitacora('Promociones', 'Registrar');
			$this->session->set_flashdata('mensaje', 'Promoción registrado exitosamente');
			redirect('promociones/listado');
		}
	}

	public function listado()
	{
		$datos['promociones'] = $this->Promociones_model->listado();
		$datos['titulo'] = 'Lista de promociones';
		$datos['contenido'] = 'promociones/listado';
		$this->load->view('administrador', $datos);
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-promociones') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('promociones/editar/' . $id);
			
		} else {
			$datos = array(
				'desc_promo' => $this->input->post('descuento'),
				'fecha_valida' => $this->input->post('valido_hasta'),
				);
			$this->Promociones_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('Promociones', 'Modificar');
			$this->session->set_flashdata('mensaje', 'Promoción modificado exitosamente');
			redirect('promociones/editar/' . $id);
		}
	}

	public function registro()
	{
		$datos['titulo'] = 'Registro de promociones';
		$datos['contenido'] = 'promociones/registro';
		$this->load->view('administrador', $datos);
	}

}
