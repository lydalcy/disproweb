<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iva extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Iva_model');
	}

	public function index()
	{
		$datos['iva'] = $this->Iva_model->consultar();
		$datos['titulo'] = 'IVA';
		$datos['contenido'] = 'iva/editar';
		$this->load->view('administrador', $datos);
	}

	public function modificar()
	{
		$id = $this->input->post('id');

		if ($this->form_validation->run('modificar-iva') == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('iva');
			
		} else {
			$datos = array(
				'iva' => $this->input->post('iva')
				);
			$this->Iva_model->modificar($id, $datos);
			$this->Usuarios_model->bitacora('IVA', 'Modificar');
			$this->session->set_flashdata('mensaje', 'IVA modificado exitosamente');
			redirect('iva');
		}
	}

}
