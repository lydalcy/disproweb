<?php

$config = array(

	// Formulario de registro de clientes
	'registro-clientes' => array(
        array(
            'field' => 'cedula',
            'label' => 'Cédula',
            'rules' => 'required|integer|is_unique[clientes.cedula]|max_length[15]'
            ),
		array(
            'field' => 'usuario',
            'label' => 'Usuario',
            'rules' => 'required|is_unique[clientes.usuario]|max_length[15]'
            ),
        array(
            'field' => 'password',
            'label' => 'Contraseña',
            'rules' => 'required'
            ),
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'apellido',
            'label' => 'Apellido',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'correo',
            'label' => 'Correo',
            'rules' => 'required|max_length[100]|valid_email|is_unique[clientes.correo]'
            ),
        array(
            'field' => 'telefono',
            'label' => 'Teléfono',
            'rules' => 'max_length[20]'
            )
        ),
    // Formulario de modificacion de clientes
    'modificar-clientes' => array(
        array(
            'field' => 'cedula',
            'label' => 'Cédula',
            'rules' => 'required|integer|max_length[15]'
            ),
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'apellido',
            'label' => 'Apellido',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'correo',
            'label' => 'Correo',
            'rules' => 'required|max_length[100]|valid_email'
            ),
        array(
            'field' => 'telefono',
            'label' => 'Teléfono',
            'rules' => 'max_length[20]'
            )
        ),
    // Formulario de registro de hostings
    'registro-hostings' => array(
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|is_unique[planes.nombre]|max_length[50]'
            ),
        array(
            'field' => 'espacio',
            'label' => 'Espacio en disco',
            'rules' => 'required'
            ),
        array(
            'field' => 'banda',
            'label' => 'Ancho de banda',
            'rules' => 'required'
            ),
        array(
            'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required'
            ),
        array(
            'field' => 'correos',
            'label' => 'Cantidad de correos',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'bases_datos',
            'label' => 'Cantidad de bases de datos',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'subdominios',
            'label' => 'Cantidad de subdominios',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'ftp',
            'label' => 'Cantidad de cuentas FTP',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'mensual',
            'label' => 'Precio mensual',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'trimestral',
            'label' => 'Precio trimestral',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'semestral',
            'label' => 'Precio semestral',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'anual',
            'label' => 'Precio anual',
            'rules' => 'required|numeric'
            ),
        ),
    // Formulario de modificaion de hostings
    'modificar-hostings' => array(
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|max_length[50]'
            ),
        array(
            'field' => 'espacio',
            'label' => 'Espacio en disco',
            'rules' => 'required'
            ),
        array(
            'field' => 'banda',
            'label' => 'Ancho de banda',
            'rules' => 'required'
            ),
        array(
            'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required'
            ),
        array(
            'field' => 'correos',
            'label' => 'Cantidad de correos',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'bases_datos',
            'label' => 'Cantidad de bases de datos',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'subdominios',
            'label' => 'Cantidad de subdominios',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'ftp',
            'label' => 'Cantidad de cuentas FTP',
            'rules' => 'required|integer'
            ),
        array(
            'field' => 'mensual',
            'label' => 'Precio mensual',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'trimestral',
            'label' => 'Precio trimestral',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'semestral',
            'label' => 'Precio semestral',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'anual',
            'label' => 'Precio anual',
            'rules' => 'required|numeric'
            ),
        ),
    // Formulario de registro de dominios
    'registro-dominios' => array(
        array(
            'field' => 'extension',
            'label' => 'Extensión',
            'rules' => 'required|is_unique[dominios.extension]|max_length[7]'
            ),
        array(
            'field' => 'precio_registro',
            'label' => 'Precio de registro',
            'rules' => 'required'
            ),
        array(
            'field' => 'precio_transferencia',
            'label' => 'Precio de transferencia',
            'rules' => 'required'
            ),
        array(
            'field' => 'precio_propio',
            'label' => 'Precio propio',
            'rules' => 'required'
            )
        ),
    // Formulario de registro de dominios
    'modificar-dominios' => array(
        array(
            'field' => 'extension',
            'label' => 'Extensión',
            'rules' => 'required|max_length[7]'
            ),
        array(
            'field' => 'precio_registro',
            'label' => 'Precio de registro',
            'rules' => 'required'
            ),
        array(
            'field' => 'precio_transferencia',
            'label' => 'Precio de transferencia',
            'rules' => 'required'
            ),
        array(
            'field' => 'precio_propio',
            'label' => 'Precio propio',
            'rules' => 'required'
            )
        ),
    // Formulario de registro de usuarios
    'registro-usuarios' => array(
        array(
            'field' => 'usuario',
            'label' => 'Usuario',
            'rules' => 'required|is_unique[usuarios.usuario]|max_length[15]'
            ),
        array(
            'field' => 'password',
            'label' => 'Contraseña',
            'rules' => 'required'
            ),
        array(
            'field' => 'nivel',
            'label' => 'Nivel de acceso',
            'rules' => 'required'
            ),
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'apellido',
            'label' => 'Apellido',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'correo',
            'label' => 'Correo',
            'rules' => 'required|max_length[100]|valid_email|is_unique[usuarios.correo]'
            ),
        ),
    // Formulario de modificacion de usuarios
    'modificar-usuarios' => array(
        array(
            'field' => 'nivel',
            'label' => 'Nivel de acceso',
            'rules' => 'required'
            ),
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'apellido',
            'label' => 'Apellido',
            'rules' => 'required|max_length[100]'
            ),
        array(
            'field' => 'correo',
            'label' => 'Correo',
            'rules' => 'required|max_length[100]|valid_email'
            ),
        ),
    // Formulario de registro de promociones
    'registro-promociones' => array(
        array(
            'field' => 'descuento',
            'label' => 'Descuento',
            'rules' => 'required|numeric'
            )
        ),
    // Formulario de modificacion de promociones
    'modificar-promociones' => array(
        array(
            'field' => 'descuento',
            'label' => 'Descuento',
            'rules' => 'required|numeric'
            )
        ),
    // Agregar al carrito
    'agregar-carrito' => array(
        array(
            'field' => 'dominio',
            'label' => 'Dominio',
            'rules' => 'required'
            ),
        ),
    // Reportar pago
    'reportar-pago' => array(
        array(
            'field' => 'banco',
            'label' => 'Banco',
            'rules' => 'required'
            ),
        array(
            'field' => 'referencia',
            'label' => 'Referencia',
            'rules' => 'required'
            ),
        array(
            'field' => 'monto',
            'label' => 'Monto',
            'rules' => 'required|numeric'
            ),
        array(
            'field' => 'fecha',
            'label' => 'Fecha',
            'rules' => 'required'
            ),
        array(
            'field' => 'factura[]',
            'label' => 'Factura',
            'rules' => 'required'
            )
    ),
    // Registrar bancos
    'registrar-bancos' => array(
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required'
            ),
        array(
            'field' => 'nro_cuenta',
            'label' => 'Nro. de Cuenta',
            'rules' => 'required|is_unique[bancos.nro_cuenta]|integer|exact_length[20]'
            ),
        array(
            'field' => 'razon_social',
            'label' => 'Razón Social',
            'rules' => 'required'
            ),
        array(
            'field' => 'rif',
            'label' => 'RIF',
            'rules' => 'required'
            ),
        ),
    // Modificar bancos
    'modificar-bancos' => array(
        array(
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required'
            ),
        array(
            'field' => 'nro_cuenta',
            'label' => 'Nro. de Cuenta',
            'rules' => 'required|integer|exact_length[20]'
            ),
        array(
            'field' => 'razon_social',
            'label' => 'Razón Social',
            'rules' => 'required'
            ),
        array(
            'field' => 'rif',
            'label' => 'RIF',
            'rules' => 'required'
            ),
        ),
    // Modificar IVA
    'modificar-iva' => array(
        array(
            'field' => 'iva',
            'label' => 'IVA',
            'rules' => 'required|numeric'
            )
        ),
);

$config['error_prefix'] = '<div class="alert alert-danger">';
$config['error_suffix'] = '</div>';