<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas_model extends CI_Model
{
	public function consultar($id)
	{
		$this->db->from('factura');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function listadoDetallesFactura($pago_id)
	{
		$this->db->from('detalle_factura');
		$this->db->where('pagos_id', $pago_id);
		$this->db->join('factura', 'detalle_factura.factura_id = factura.id');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function facturasCliente($cliente_id, $estado = 1)
	{
		$this->db->select('factura.id, descuento, fecha_creacion, estado_id, pedidos.id as pedido_id');
		$this->db->from('factura');
		$this->db->where('cliente_id', $cliente_id);
		if (!is_null($estado)) {
			$this->db->where('estado_id', $estado);
		}
		$this->db->join('pedidos', 'factura.pedidos_id = pedidos.id');
		$this->db->order_by('factura.id', 'desc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function guardar($datos)
	{
		$this->db->insert('factura', $datos);
		$factura = $this->db->insert_id();
		return $factura;
	}

	public function listado($estado = null)
	{
		$this->db->select('factura.id, descuento,fecha_creacion, estado_id, cedula, nombre, apellido, correo, pedidos.id as pedido_id');
		$this->db->from('factura');
		if (!is_null($estado)) {
			$this->db->where('estado_id', $estado);
		}
		$this->db->join('pedidos', 'factura.pedidos_id = pedidos.id');
		$this->db->join('clientes', 'pedidos.cliente_id = clientes.id');
		$this->db->order_by('factura.id', 'desc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function listadoPorFecha($fecha_inicial, $fecha_final)
	{
		$this->db->select('factura.id, descuento,fecha_creacion, estado_id, cedula, nombre, apellido, correo, pedidos.id as pedido_id');
		$this->db->from('factura');
		$this->db->where('fecha_creacion >=', $fecha_inicial);
		$this->db->where('fecha_creacion <=', $fecha_final);
		$this->db->join('pedidos', 'factura.pedidos_id = pedidos.id');
		$this->db->join('clientes', 'pedidos.cliente_id = clientes.id');
		$this->db->order_by('factura.id', 'desc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($factura_id, $datos)
	{
		$this->db->where('id', $factura_id);
		$this->db->update('factura', $datos);
	}
}
