<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model
{
	public function bitacora($modulo, $accion)
	{
		$datos = array(
			'modulo' => $modulo,
			'accion' => $accion,
			'fecha' => date('Y-m-d H:i:s'),
			'usuario_id' => $this->session->userdata('id')
			);
		$this->db->insert('bitacora', $datos);
	}

	public function consultar($id)
	{
		$this->db->from('usuarios');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function eliminar($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('usuarios');
	}

	public function listado()
	{
		$this->db->select('usuarios.id, usuario, correo, usuarios.nombre, apellido, niveles.nombre as nombre_nivel');
		$this->db->from('usuarios');
		$this->db->join('niveles', 'usuarios.nivel_id = niveles.id');
		$this->db->order_by('usuario', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function login($usuario, $password)
	{
		$this->db->from('usuarios');
		$this->db->where('usuario', $usuario);
		$this->db->where('password', $password);
		$consulta = $this->db->get();

		if ($consulta->num_rows() > 0) {
			$datos_usuarios = $consulta->row();
			$this->session->set_userdata('id', $datos_usuarios->id);
			$this->session->set_userdata('usuario', $usuario);
			$this->session->set_userdata('nivel', $datos_usuarios->nivel_id);
			$this->bitacora('Usuarios', 'Iniciar sesión');
			return true;
		} else {
			return false;
		}
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('usuarios', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('usuarios', $datos);
	}
}
