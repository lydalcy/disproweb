<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promociones_model extends CI_Model
{	
	public function consultar($id)
	{
		$this->db->from('promociones');
		$this->db->where('id', $id);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function consultarPorCodigo($codigo)
	{
		$this->db->from('promociones');
		$this->db->where('codigo_cupon', $codigo);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function eliminar($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('promociones');
	}

	public function listado()
	{
		$this->db->from('promociones');
		$this->db->order_by('codigo_cupon', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function modificar($id, $datos)
	{
		$this->db->where('id', $id);
		$this->db->update('promociones', $datos);
	}

	public function registrar($datos)
	{
		$this->db->insert('promociones', $datos);
	}
}
