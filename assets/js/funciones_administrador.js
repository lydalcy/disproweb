$(function() {
	$('#cedula').mask('00000000');

	$('#telefono').mask('(0000)-0000000');
	
	// Configura los tooltips
	$('[data-tooltip="tooltip"]').tooltip();

	$('.datatables-hostings').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-dominios').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-usuarios').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-clientes').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-promociones').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-facturas').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	$('.datatables-pagos').dataTable({
		"order": [],
		"language": {
			"url": "/assets/i18n/datatables/es_ES.lang"
		}
	});

	// Configura el modal al abrirlo
	$('#modal-confirmacion').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var route = button.data('route');
		$('#confirmar').attr('href', route);
	});
});