# ===================================================================
#  mwBackup ver.1.2
#  Copias de seguridad
#  
#  Servidor: disproweb.dev 
#  Base de datos: `disproweb` 
#  
# -------------------------------------------------------------------
# TABLAS:
#  17
# -------------------------------------------------------------------
#
#Se borra la base de datos y se vuelve a crear
#
DROP DATABASE IF EXISTS `disproweb`; 
CREATE DATABASE `disproweb`; 
USE `disproweb`; 
#
#
#  Estructura de la tabla `niveles` 
# ------------------------------------- 
#
#
CREATE TABLE `niveles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `niveles` 
# ------------------------------------- 
#
#
INSERT INTO `niveles` VALUES ('1', 'Administrador');
INSERT INTO `niveles` VALUES ('2', 'Operador');
#
#
#  Estructura de la tabla `bitacora` 
# ------------------------------------- 
#
#
CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulo` varchar(50) DEFAULT NULL,
  `accion` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bitacora_usuarios1_idx` (`usuario_id`),
  CONSTRAINT `fk_bitacora_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `bitacora` 
# ------------------------------------- 
#
#
INSERT INTO `bitacora` VALUES ('1', 'Usuarios', 'Iniciar sesi�n', '2016-01-24 17:46:36', '1');
INSERT INTO `bitacora` VALUES ('2', 'Hostings', 'Registrar', '2016-01-24 17:48:27', '1');
INSERT INTO `bitacora` VALUES ('3', 'Hostings', 'Registrar', '2016-01-24 17:49:02', '1');
INSERT INTO `bitacora` VALUES ('4', 'Dominios', 'Registrar', '2016-01-24 17:50:06', '1');
INSERT INTO `bitacora` VALUES ('5', 'Dominios', 'Registrar', '2016-01-24 17:50:21', '1');
INSERT INTO `bitacora` VALUES ('6', 'Dominios', 'Registrar', '2016-01-24 17:50:50', '1');
INSERT INTO `bitacora` VALUES ('7', 'Usuarios', 'Iniciar sesi�n', '2016-01-24 19:30:04', '1');
INSERT INTO `bitacora` VALUES ('8', 'Usuarios', 'Iniciar sesi�n', '2016-01-31 09:51:12', '1');
INSERT INTO `bitacora` VALUES ('9', 'Promociones', 'Registrar', '2016-01-31 09:53:58', '1');
INSERT INTO `bitacora` VALUES ('10', 'Usuarios', 'Iniciar sesi�n', '2016-01-31 10:40:25', '1');
INSERT INTO `bitacora` VALUES ('11', 'Dominios', 'Modificar', '2016-01-31 10:40:36', '1');
INSERT INTO `bitacora` VALUES ('12', 'Dominios', 'Modificar', '2016-01-31 10:51:46', '1');
INSERT INTO `bitacora` VALUES ('13', 'Dominios', 'Modificar', '2016-01-31 10:59:08', '1');
INSERT INTO `bitacora` VALUES ('14', 'Dominios', 'Modificar', '2016-01-31 10:59:28', '1');
INSERT INTO `bitacora` VALUES ('15', 'Dominios', 'Modificar', '2016-01-31 10:59:36', '1');
INSERT INTO `bitacora` VALUES ('16', 'Dominios', 'Modificar', '2016-01-31 11:08:20', '1');
INSERT INTO `bitacora` VALUES ('17', 'Usuarios', 'Iniciar sesi�n', '2016-02-02 21:31:06', '1');
INSERT INTO `bitacora` VALUES ('18', 'Usuarios', 'Iniciar sesi�n', '2016-02-02 21:44:59', '1');
INSERT INTO `bitacora` VALUES ('19', 'Usuarios', 'Iniciar sesi�n', '2016-02-03 23:16:45', '1');
INSERT INTO `bitacora` VALUES ('20', 'Bancos', 'Registrar', '2016-02-03 23:22:18', '1');
INSERT INTO `bitacora` VALUES ('21', 'Bancos', 'Modificar', '2016-02-03 23:28:04', '1');
INSERT INTO `bitacora` VALUES ('22', 'Bancos', 'Modificar', '2016-02-03 23:28:15', '1');
INSERT INTO `bitacora` VALUES ('23', 'Bancos', 'Eliminar', '2016-02-03 23:29:18', '1');
INSERT INTO `bitacora` VALUES ('24', 'Usuarios', 'Registrar', '2016-02-03 23:34:30', '1');
INSERT INTO `bitacora` VALUES ('25', 'Usuarios', 'Cerrar sesi�n', '2016-02-03 23:34:34', '1');
INSERT INTO `bitacora` VALUES ('26', 'Usuarios', 'Iniciar sesi�n', '2016-02-03 23:34:41', '2');
INSERT INTO `bitacora` VALUES ('27', 'Usuarios', 'Cerrar sesi�n', '2016-02-03 23:38:25', '2');
INSERT INTO `bitacora` VALUES ('28', 'Usuarios', 'Iniciar sesi�n', '2016-02-07 23:47:56', '1');
INSERT INTO `bitacora` VALUES ('29', 'Usuarios', 'Cerrar sesi�n', '2016-02-07 23:49:28', '1');
INSERT INTO `bitacora` VALUES ('30', 'Usuarios', 'Iniciar sesi�n', '2016-02-07 23:52:13', '1');
INSERT INTO `bitacora` VALUES ('31', 'Usuarios', 'Cerrar sesi�n', '2016-02-07 23:52:17', '1');
INSERT INTO `bitacora` VALUES ('32', 'Usuarios', 'Iniciar sesi�n', '2016-02-07 23:52:22', '2');
INSERT INTO `bitacora` VALUES ('33', 'Usuarios', 'Cerrar sesi�n', '2016-02-07 23:52:25', '2');
INSERT INTO `bitacora` VALUES ('34', 'Usuarios', 'Iniciar sesi�n', '2016-02-07 23:52:34', '1');
INSERT INTO `bitacora` VALUES ('35', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 00:27:36', '1');
INSERT INTO `bitacora` VALUES ('36', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 00:31:30', '1');
INSERT INTO `bitacora` VALUES ('37', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 00:31:51', '1');
INSERT INTO `bitacora` VALUES ('38', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 00:31:57', '1');
INSERT INTO `bitacora` VALUES ('39', 'Promociones', 'Registrar', '2016-02-08 00:40:45', '1');
INSERT INTO `bitacora` VALUES ('40', 'IVA', 'Modificar', '2016-02-08 01:12:49', '1');
INSERT INTO `bitacora` VALUES ('41', 'IVA', 'Modificar', '2016-02-08 01:12:53', '1');
INSERT INTO `bitacora` VALUES ('42', 'IVA', 'Modificar', '2016-02-08 01:12:57', '1');
INSERT INTO `bitacora` VALUES ('43', 'IVA', 'Modificar', '2016-02-08 01:13:02', '1');
INSERT INTO `bitacora` VALUES ('44', 'IVA', 'Modificar', '2016-02-08 01:13:08', '1');
INSERT INTO `bitacora` VALUES ('45', 'IVA', 'Modificar', '2016-02-08 01:13:10', '1');
INSERT INTO `bitacora` VALUES ('46', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 01:13:28', '1');
INSERT INTO `bitacora` VALUES ('47', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 01:34:17', '1');
INSERT INTO `bitacora` VALUES ('48', 'IVA', 'Modificar', '2016-02-08 01:34:29', '1');
INSERT INTO `bitacora` VALUES ('49', 'IVA', 'Modificar', '2016-02-08 01:34:49', '1');
INSERT INTO `bitacora` VALUES ('50', 'IVA', 'Modificar', '2016-02-08 01:34:55', '1');
INSERT INTO `bitacora` VALUES ('51', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 01:34:59', '1');
INSERT INTO `bitacora` VALUES ('52', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 18:44:11', '1');
INSERT INTO `bitacora` VALUES ('53', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 18:50:16', '1');
INSERT INTO `bitacora` VALUES ('54', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 18:52:06', '1');
INSERT INTO `bitacora` VALUES ('55', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 19:00:14', '1');
INSERT INTO `bitacora` VALUES ('56', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 19:00:22', '1');
INSERT INTO `bitacora` VALUES ('57', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:13:21', '1');
INSERT INTO `bitacora` VALUES ('58', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:23:06', '1');
INSERT INTO `bitacora` VALUES ('59', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:23:26', '1');
INSERT INTO `bitacora` VALUES ('60', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:23:31', '2');
INSERT INTO `bitacora` VALUES ('61', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:23:37', '2');
INSERT INTO `bitacora` VALUES ('62', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:23:41', '1');
INSERT INTO `bitacora` VALUES ('63', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:23:58', '1');
INSERT INTO `bitacora` VALUES ('64', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:25:20', '1');
INSERT INTO `bitacora` VALUES ('65', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:26:41', '1');
INSERT INTO `bitacora` VALUES ('66', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:27:05', '1');
INSERT INTO `bitacora` VALUES ('67', 'IVA', 'Modificar', '2016-02-08 21:27:13', '1');
INSERT INTO `bitacora` VALUES ('68', 'IVA', 'Modificar', '2016-02-08 21:27:26', '1');
INSERT INTO `bitacora` VALUES ('69', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 21:52:14', '1');
INSERT INTO `bitacora` VALUES ('70', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 21:52:42', '1');
INSERT INTO `bitacora` VALUES ('71', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 22:14:24', '1');
INSERT INTO `bitacora` VALUES ('72', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 22:17:01', '1');
INSERT INTO `bitacora` VALUES ('73', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 22:28:37', '1');
INSERT INTO `bitacora` VALUES ('74', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 22:29:06', '1');
INSERT INTO `bitacora` VALUES ('75', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 23:02:14', '1');
INSERT INTO `bitacora` VALUES ('76', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 23:23:18', '1');
INSERT INTO `bitacora` VALUES ('77', 'Usuarios', 'Cerrar sesi�n', '2016-02-08 23:23:52', '1');
INSERT INTO `bitacora` VALUES ('78', 'Usuarios', 'Iniciar sesi�n', '2016-02-08 23:36:06', '1');
INSERT INTO `bitacora` VALUES ('79', 'Bancos', 'Modificar', '2016-02-08 23:53:35', '1');
INSERT INTO `bitacora` VALUES ('80', 'Bancos', 'Modificar', '2016-02-08 23:57:02', '1');
INSERT INTO `bitacora` VALUES ('81', 'Bancos', 'Modificar', '2016-02-08 23:57:22', '1');
INSERT INTO `bitacora` VALUES ('82', 'Bancos', 'Registrar', '2016-02-08 23:58:04', '1');
INSERT INTO `bitacora` VALUES ('83', 'Bancos', 'Eliminar', '2016-02-08 23:58:52', '1');
INSERT INTO `bitacora` VALUES ('84', 'Usuarios', 'Iniciar sesi�n', '2016-02-09 23:14:23', '1');
INSERT INTO `bitacora` VALUES ('85', 'Hostings', 'Modificar', '2016-02-09 23:15:26', '1');
INSERT INTO `bitacora` VALUES ('86', 'Dominios', 'Registrar', '2016-02-09 23:18:39', '1');
INSERT INTO `bitacora` VALUES ('87', 'Usuarios', 'Iniciar sesi�n', '2016-02-15 01:35:57', '1');
INSERT INTO `bitacora` VALUES ('88', 'Usuarios', 'Cerrar sesi�n', '2016-02-15 01:46:22', '1');
INSERT INTO `bitacora` VALUES ('89', 'Usuarios', 'Iniciar sesi�n', '2016-02-15 01:46:39', '1');
INSERT INTO `bitacora` VALUES ('90', 'Usuarios', 'Iniciar sesi�n', '2016-02-24 01:49:31', '1');
INSERT INTO `bitacora` VALUES ('91', 'Usuarios', 'Iniciar sesi�n', '2016-02-24 02:20:51', '1');
#
#
#  Estructura de la tabla `usuarios` 
# ------------------------------------- 
#
#
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `nivel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`usuario`),
  UNIQUE KEY `email_UNIQUE` (`correo`),
  KEY `fk_usuarios_nivel1_idx` (`nivel_id`),
  CONSTRAINT `fk_usuarios_nivel1` FOREIGN KEY (`nivel_id`) REFERENCES `niveles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `usuarios` 
# ------------------------------------- 
#
#
INSERT INTO `usuarios` VALUES ('1', 'administrador', 'e10adc3949ba59abbe56e057f20f883e', 'admin@disproweb.net', 'Admin', 'Disproweb', '1');
INSERT INTO `usuarios` VALUES ('2', 'operador', 'e10adc3949ba59abbe56e057f20f883e', 'operador@disproweb.net', 'Operador', 'Disproweb', '2');
#
#
#  Estructura de la tabla `estados_facturas` 
# ------------------------------------- 
#
#
CREATE TABLE `estados_facturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `estados_facturas` 
# ------------------------------------- 
#
#
INSERT INTO `estados_facturas` VALUES ('1', 'Pendiente');
INSERT INTO `estados_facturas` VALUES ('2', 'Pagada');
INSERT INTO `estados_facturas` VALUES ('3', 'Cancelada');
#
#
#  Estructura de la tabla `iva` 
# ------------------------------------- 
#
#
CREATE TABLE `iva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iva` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `iva` 
# ------------------------------------- 
#
#
INSERT INTO `iva` VALUES ('1', '12');
#
#
#  Estructura de la tabla `bancos` 
# ------------------------------------- 
#
#
CREATE TABLE `bancos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `nro_cuenta` varchar(20) DEFAULT NULL,
  `razon_social` varchar(150) DEFAULT NULL,
  `rif` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `bancos` 
# ------------------------------------- 
#
#
INSERT INTO `bancos` VALUES ('1', 'Mercantil', '01050737560737039698', 'Disproweb C.A.', 'J-40101414-3');
INSERT INTO `bancos` VALUES ('2', 'Banesco', '01340416129584739150', 'Disproweb C.A.', 'J-40101414-3');
INSERT INTO `bancos` VALUES ('3', 'Provincial', '01180001849372839191', 'Disproweb C.A.', 'J-40101414-3');
INSERT INTO `bancos` VALUES ('4', 'BNC', '01096789788388972987', 'Disproweb C.A.', 'J-40101414-3');
#
#
#  Estructura de la tabla `estados_pagos` 
# ------------------------------------- 
#
#
CREATE TABLE `estados_pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `estados_pagos` 
# ------------------------------------- 
#
#
INSERT INTO `estados_pagos` VALUES ('1', 'Pendiente');
INSERT INTO `estados_pagos` VALUES ('2', 'Aprobado');
INSERT INTO `estados_pagos` VALUES ('3', 'Rechazado');
#
#
#  Estructura de la tabla `pagos` 
# ------------------------------------- 
#
#
CREATE TABLE `pagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referencia_trans` varchar(50) DEFAULT NULL,
  `monto_pagado` double DEFAULT NULL,
  `comentario` text,
  `fecha` date DEFAULT NULL,
  `estado_id` int(11) DEFAULT '1',
  `banco_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pagos_bancos2_idx` (`banco_id`),
  KEY `fk_pagos_estados` (`estado_id`),
  CONSTRAINT `fk_pagos_bancos2` FOREIGN KEY (`banco_id`) REFERENCES `bancos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pagos_estados` FOREIGN KEY (`estado_id`) REFERENCES `estados_pagos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `pagos` 
# ------------------------------------- 
#
#
INSERT INTO `pagos` VALUES ('14', '234234234', '4468.8', 'Factura pagada', '2016-02-08', '2', '4');
INSERT INTO `pagos` VALUES ('15', '324234', '16811.2', 'Pago', '2016-02-16', '2', '4');
#
#
#  Estructura de la tabla `factura` 
# ------------------------------------- 
#
#
CREATE TABLE `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descuento` double DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `estado_id` int(11) DEFAULT '1',
  `pedidos_id` int(11) NOT NULL,
  `iva_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_factura_ordenes1_idx` (`pedidos_id`),
  KEY `fk_factura_Iva1_idx` (`iva_id`) USING BTREE,
  KEY `fk_factura_estado` (`estado_id`),
  CONSTRAINT `fk_factura_Iva1` FOREIGN KEY (`iva_id`) REFERENCES `iva` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_estado` FOREIGN KEY (`estado_id`) REFERENCES `estados_facturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_factura_pedidos1` FOREIGN KEY (`pedidos_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `factura` 
# ------------------------------------- 
#
#
INSERT INTO `factura` VALUES ('14', '0', '2016-02-08', NULL, '1', '16', '1');
INSERT INTO `factura` VALUES ('15', '0', '2016-02-08', NULL, '1', '17', '1');
INSERT INTO `factura` VALUES ('16', '0', '2016-02-08', NULL, '1', '18', '1');
INSERT INTO `factura` VALUES ('17', '235.2', '2016-02-08', NULL, '1', '19', '1');
INSERT INTO `factura` VALUES ('18', '235.2', '2016-02-08', '2016-02-08', '2', '20', '1');
INSERT INTO `factura` VALUES ('19', '0', '2016-02-08', NULL, '1', '21', '1');
INSERT INTO `factura` VALUES ('20', '0', '2016-02-08', NULL, '1', '22', '1');
INSERT INTO `factura` VALUES ('21', '0', '2016-02-10', NULL, '1', '23', '1');
INSERT INTO `factura` VALUES ('22', '0', '2016-02-24', NULL, '1', '24', '1');
INSERT INTO `factura` VALUES ('23', '0', '2016-02-24', NULL, '1', '25', '1');
INSERT INTO `factura` VALUES ('24', '790', '2016-02-24', '2016-02-16', '2', '26', '1');
INSERT INTO `factura` VALUES ('25', '0', '2016-02-24', NULL, '1', '27', '1');
#
#
#  Estructura de la tabla `detalle_factura` 
# ------------------------------------- 
#
#
CREATE TABLE `detalle_factura` (
  `factura_id` int(11) NOT NULL,
  `pagos_id` int(11) NOT NULL,
  KEY `fk_detalle factura_factura1_idx` (`factura_id`),
  KEY `fk_detalle factura_pagos1_idx` (`pagos_id`),
  CONSTRAINT `fk_detalle factura_factura1` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_detalle factura_pagos1` FOREIGN KEY (`pagos_id`) REFERENCES `pagos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `detalle_factura` 
# ------------------------------------- 
#
#
INSERT INTO `detalle_factura` VALUES ('18', '14');
INSERT INTO `detalle_factura` VALUES ('24', '15');
#
#
#  Estructura de la tabla `dominios` 
# ------------------------------------- 
#
#
CREATE TABLE `dominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(7) DEFAULT NULL,
  `precio_registro` double DEFAULT NULL,
  `precio_transferencia` double DEFAULT NULL,
  `precio_propio` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `dominios` 
# ------------------------------------- 
#
#
INSERT INTO `dominios` VALUES ('1', '.com.ve', '1200', '600', '0');
INSERT INTO `dominios` VALUES ('2', '.net.ve', '1150', '500', '0');
INSERT INTO `dominios` VALUES ('3', '.org.ve', '2000', '1800', '0');
INSERT INTO `dominios` VALUES ('4', '.com', '15000', '14000', '0');
#
#
#  Estructura de la tabla `promociones` 
# ------------------------------------- 
#
#
CREATE TABLE `promociones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_cupon` varchar(40) DEFAULT NULL,
  `desc_promo` double DEFAULT NULL,
  `fecha_valida` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `promociones` 
# ------------------------------------- 
#
#
INSERT INTO `promociones` VALUES ('1', 'ScORuVvbp2C4JTxi', '0', '0000-00-00');
INSERT INTO `promociones` VALUES ('2', 'CuUQ0HlbcOEhokFi', '5', '0000-00-00');
#
#
#  Estructura de la tabla `clientes` 
# ------------------------------------- 
#
#
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(15) DEFAULT NULL,
  `usuario` varchar(15) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `direccion` text,
  `telefono` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`usuario`),
  UNIQUE KEY `email_UNIQUE` (`correo`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `clientes` 
# ------------------------------------- 
#
#
INSERT INTO `clientes` VALUES ('6', '18997724', 'eborio', 'e10adc3949ba59abbe56e057f20f883e', 'eboriolinarez@gmail.com', 'Eborio', 'Lin�rez', 'Cale', '04269537352');
#
#
#  Estructura de la tabla `pedidos` 
# ------------------------------------- 
#
#
CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `promocion_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ordenes_promocion1_idx` (`promocion_id`),
  KEY `fk_pedidos_clientes1_idx` (`cliente_id`),
  CONSTRAINT `fk_pedidos_clientes1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_promociones1` FOREIGN KEY (`promocion_id`) REFERENCES `promociones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `pedidos` 
# ------------------------------------- 
#
#
INSERT INTO `pedidos` VALUES ('16', '2016-02-08', '1', '6');
INSERT INTO `pedidos` VALUES ('17', '2016-02-08', '1', '6');
INSERT INTO `pedidos` VALUES ('18', '2016-02-08', '1', '6');
INSERT INTO `pedidos` VALUES ('19', '2016-02-08', '2', '6');
INSERT INTO `pedidos` VALUES ('20', '2016-02-08', '2', '6');
INSERT INTO `pedidos` VALUES ('21', '2016-02-08', '1', '6');
INSERT INTO `pedidos` VALUES ('22', '2016-02-08', '1', '6');
INSERT INTO `pedidos` VALUES ('23', '2016-02-10', '1', '6');
INSERT INTO `pedidos` VALUES ('24', '2016-02-24', '1', '6');
INSERT INTO `pedidos` VALUES ('25', '2016-02-24', '1', '6');
INSERT INTO `pedidos` VALUES ('26', '2016-02-24', '2', '6');
INSERT INTO `pedidos` VALUES ('27', '2016-02-24', '1', '6');
#
#
#  Estructura de la tabla `dominio_pedido` 
# ------------------------------------- 
#
#
CREATE TABLE `dominio_pedido` (
  `pedido_id` int(11) NOT NULL,
  `dominio_id` int(11) NOT NULL,
  `dominio` varchar(255) DEFAULT NULL,
  `fecha_expiracion` date DEFAULT NULL,
  `dns1` varchar(150) DEFAULT NULL,
  `dns2` varchar(150) DEFAULT NULL,
  `dns3` varchar(150) DEFAULT NULL,
  `dns4` varchar(150) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  KEY `fk_dominio_orden_ordenes1_idx` (`pedido_id`),
  KEY `fk_dominio_orden_dominio1_idx` (`dominio_id`),
  CONSTRAINT `fk_dominio_orden_dominio1` FOREIGN KEY (`dominio_id`) REFERENCES `dominios` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_dominio_orden_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `dominio_pedido` 
# ------------------------------------- 
#
#
INSERT INTO `dominio_pedido` VALUES ('16', '1', 'www.todoprogramacion.com.ve', '2016-02-08', '', '', '', '', '1200');
INSERT INTO `dominio_pedido` VALUES ('17', '1', 'www.disproweb.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('18', '1', 'www.todoprogramacion.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('19', '1', 'www.todoprogramacion.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('20', '1', 'www.todoprogramacion.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('21', '1', 'www.todoprogramacion.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('22', '1', 'www.disproweb.com.ve', '2016-02-08', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('23', '4', 'www.disproweb.com', '2017-02-10', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('24', '4', 'www.todoprogramacion.com', '2017-02-24', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('25', '4', 'www.todoprogramacion.com', '2017-02-24', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('26', '4', 'www.todoprogramacion.com', '2017-02-24', '', '', '', '', 'Registro');
INSERT INTO `dominio_pedido` VALUES ('27', '4', 'www.todoprogramacion.com', '2017-02-24', '', '', '', '', 'Registro');
#
#
#  Estructura de la tabla `planes` 
# ------------------------------------- 
#
#
CREATE TABLE `planes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` text,
  `espacio` double DEFAULT NULL,
  `banda_ancha` double DEFAULT NULL,
  `cant_correos` int(11) DEFAULT NULL,
  `cant_subdominios` int(11) DEFAULT NULL,
  `cant_basedatos` int(11) DEFAULT NULL,
  `cant_ftp` int(11) DEFAULT NULL,
  `precio_mensual` double DEFAULT NULL,
  `precio_trimestral` double DEFAULT NULL,
  `precio_semestral` double DEFAULT NULL,
  `precio_anual` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `planes` 
# ------------------------------------- 
#
#
INSERT INTO `planes` VALUES ('1', 'Inicio', 'Plan para principiantes', '3', '100', '3', '2', '3', '1', '800', '2000', '4000', '7000');
INSERT INTO `planes` VALUES ('2', 'B�sico', 'Plan b�sico', '0', '200', '10', '5', '5', '5', '3000', '9000', '12000', '16000');
#
#
#  Estructura de la tabla `planes_pedidos` 
# ------------------------------------- 
#
#
CREATE TABLE `planes_pedidos` (
  `pedidos_id` int(11) NOT NULL,
  `planes_id` int(11) NOT NULL,
  `fecha_expiracion` date DEFAULT NULL,
  `ciclo_facturacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pedidos_id`,`planes_id`),
  KEY `fk_hosting_orden_ordenes1_idx` (`pedidos_id`),
  KEY `fk_hosting_orden_hostings1_idx` (`planes_id`),
  CONSTRAINT `fk_hosting_orden_hostings1` FOREIGN KEY (`planes_id`) REFERENCES `planes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_hosting_orden_pedidos1` FOREIGN KEY (`pedidos_id`) REFERENCES `pedidos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#
#
#  Carga de datos de la tabla `planes_pedidos` 
# ------------------------------------- 
#
#
INSERT INTO `planes_pedidos` VALUES ('16', '1', '2016-02-08', '800');
INSERT INTO `planes_pedidos` VALUES ('17', '2', '2016-02-08', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('18', '2', '2016-02-08', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('19', '2', '2016-02-08', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('20', '2', '2016-02-08', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('22', '2', '2016-02-08', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('23', '1', '2016-02-10', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('24', '1', '2016-03-24', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('25', '1', '2016-05-24', 'Trimestral');
INSERT INTO `planes_pedidos` VALUES ('26', '1', '2016-03-24', 'Mensual');
INSERT INTO `planes_pedidos` VALUES ('27', '1', '2016-03-24', 'Mensual');
